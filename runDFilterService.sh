#!/bin/sh

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_77.jdk/Contents/Home
export PATH=$JAVA_HOME/bin:$PATH

#export JAVA_HOME=$(/Library/Java/JavaVirtualMachines/jdk1.8.0_77.jdk/Contents/Home/bin/java)

#---------------------------------#
# dynamically build the classpath #
#---------------------------------#

cp target/DFilterServer.jar .

THE_CLASSPATH=
for i in `ls *.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done

export THE_CLASSPATH=${THE_CLASSPATH}:reference.conf:/mnt/region:

echo ${THE_CLASSPATH}

java -server -XX:-MaxFDLimit -XX:+UseConcMarkSweepGC -XX:+UseParNewGC \
-XX:MaxTenuringThreshold=4 -XX:SurvivorRatio=6 \
-XX:TargetSurvivorRatio=90  -XX:NewSize=5120m -XX:MaxNewSize=5120m -Xms4511m -Xmx10527m \
-Dio.netty.leakDetectionLevel=DISABLED \
-Dnet.spy.log.LoggerImpl=net.spy.memcached.compat.log.Log4JLogger \
-javaagent:/opt/hudson/spring-agent-2.5.6.jar \
-cp ".:${THE_CLASSPATH}"  \
 com.vdopia.bootstrap.DFilterServer \
