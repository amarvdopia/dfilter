package com.vdopia.dfilter;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vdopia.aws.DeserializerException;
import com.vdopia.bootstrap.Configuration;
import com.vdopia.data.DFilterObjectStore;
import com.vdopia.data.DFilterSettingsObjectStore;
import com.vdopia.data.ObjectEnum;
import com.vdopia.data.ObjectStoreFactory;

public class TestDFilterStore {

	private static final Logger logger = LoggerFactory.getLogger(TestDFilterStore.class.getName());

	//0bbc8e29-1eab-4726-a857-4509d974e09c_com.cleanmaster.mguard_0W12i9_USA_13_3_-_
	@Test
	public void testDFilterStore() {
		Gson gson = new Gson();
		String bidderJson="{\"bidders\":[\"0bbc8e29-1eab-4726-a857-4509d974e09c\"],"
				+ "\"apikey\":\"0W12i9\",\"country\":\"USA\","
				+ "\"propertyName\":\"com.cleanmaster.mguard\",\"locationCode\":\"\"}";
		
		DFilterInputData dfilterInput = gson.fromJson(bidderJson, new TypeToken<DFilterInputData>(){}.getType());
	
		assertEquals("0W12i9",dfilterInput.getApikey());
		assertEquals(1,dfilterInput.getBidders().size());
		
		
		List<String> regionList = Configuration.getConfig().getStringList("dfilterserver.region");
		
		ObjectStoreFactory objectStoreFactory = new ObjectStoreFactory(regionList);
		
		DFilterObjectStore objectStore = (DFilterObjectStore) objectStoreFactory.getConfigDataStore(ObjectEnum.DFILTER);
		objectStore.populateDataStore(null);
		
		DFilterSettingsObjectStore dsobjectStore = (DFilterSettingsObjectStore) objectStoreFactory.getConfigDataStore(ObjectEnum.DFILTERSETTING);
		try {
			dsobjectStore.loadDataStore();
		} catch (DeserializerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		DFilterHandler dhandler = new DFilterHandler(objectStoreFactory);
		
		List<String> selectedBidders = dhandler.getBidders(dfilterInput);
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonResponse =  mapper.writeValueAsString(selectedBidders);
			logger.info("jsonResponse {}",jsonResponse);
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		assertEquals(1,selectedBidders.size());
		

	}
	
	//{"bidderId":["testbidder1","testbidder2"],"apiKey":"tapikey","country":"USA","propertyName":"test.com","locationCode":"CA95014"}

	/*
	@Test
	public void testDFilterJson(){
		
		Gson gson = new Gson();
		String bidderJson="{\"bidders\":[\"16ec4172-2df1-4ef4-9ec7-eec836af9c13\"],"
				+ "\"apikey\":\"0JYosV\",\"country\":\"USA\","
				+ "\"propertyName\":\"com.cleanmaster.mguard\",\"locationCode\":\"\"}";
	
		
		DFilterInputData dfilterInput = gson.fromJson(bidderJson, new TypeToken<DFilterInputData>(){}.getType());
		
		assertEquals(1,dfilterInput.getBidders().size());
		
	}
	*/
	
}
