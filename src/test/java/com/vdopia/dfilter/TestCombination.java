package com.vdopia.dfilter;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.paukov.combinatorics3.Generator;
import org.paukov.combinatorics3.IGenerator;

public class TestCombination {

	@Test
	public void testCombinations() {

		int count =0;
		IGenerator<List<String>>  resList = Generator.permutation("One", "black", "white")
	       .simple();
	       //.stream()
	       //.forEach(System.out::println);
		
		for(List<String> combination:resList){
			count++;
		}
		
		assertEquals(6,count);
		
	}
}
