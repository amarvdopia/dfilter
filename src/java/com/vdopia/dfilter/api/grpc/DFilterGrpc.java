package com.vdopia.dfilter.api.grpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 * <pre>
 * Interface exported by the server.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.0.3)",
    comments = "Source: dfilter.proto")
public class DFilterGrpc {

  private DFilterGrpc() {}

  public static final String SERVICE_NAME = "dfilterproto.DFilter";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.vdopia.dfilter.api.grpc.DFilterInput,
      com.vdopia.dfilter.api.grpc.BidderSelected> METHOD_GET_BIDDERS =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "dfilterproto.DFilter", "GetBidders"),
          io.grpc.protobuf.ProtoUtils.marshaller(com.vdopia.dfilter.api.grpc.DFilterInput.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(com.vdopia.dfilter.api.grpc.BidderSelected.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DFilterStub newStub(io.grpc.Channel channel) {
    return new DFilterStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DFilterBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DFilterBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static DFilterFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DFilterFutureStub(channel);
  }

  /**
   * <pre>
   * Interface exported by the server.
   * </pre>
   */
  public static abstract class DFilterImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * A simple RPC.
     * </pre>
     */
    public void getBidders(com.vdopia.dfilter.api.grpc.DFilterInput request,
        io.grpc.stub.StreamObserver<com.vdopia.dfilter.api.grpc.BidderSelected> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_BIDDERS, responseObserver);
    }

    @java.lang.Override public io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_BIDDERS,
            asyncUnaryCall(
              new MethodHandlers<
                com.vdopia.dfilter.api.grpc.DFilterInput,
                com.vdopia.dfilter.api.grpc.BidderSelected>(
                  this, METHODID_GET_BIDDERS)))
          .build();
    }
  }

  /**
   * <pre>
   * Interface exported by the server.
   * </pre>
   */
  public static final class DFilterStub extends io.grpc.stub.AbstractStub<DFilterStub> {
    private DFilterStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DFilterStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DFilterStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DFilterStub(channel, callOptions);
    }

    /**
     * <pre>
     * A simple RPC.
     * </pre>
     */
    public void getBidders(com.vdopia.dfilter.api.grpc.DFilterInput request,
        io.grpc.stub.StreamObserver<com.vdopia.dfilter.api.grpc.BidderSelected> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_BIDDERS, getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Interface exported by the server.
   * </pre>
   */
  public static final class DFilterBlockingStub extends io.grpc.stub.AbstractStub<DFilterBlockingStub> {
    private DFilterBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DFilterBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DFilterBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DFilterBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * A simple RPC.
     * </pre>
     */
    public com.vdopia.dfilter.api.grpc.BidderSelected getBidders(com.vdopia.dfilter.api.grpc.DFilterInput request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_BIDDERS, getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Interface exported by the server.
   * </pre>
   */
  public static final class DFilterFutureStub extends io.grpc.stub.AbstractStub<DFilterFutureStub> {
    private DFilterFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DFilterFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DFilterFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DFilterFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * A simple RPC.
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.vdopia.dfilter.api.grpc.BidderSelected> getBidders(
        com.vdopia.dfilter.api.grpc.DFilterInput request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_BIDDERS, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_BIDDERS = 0;

  private static class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DFilterImplBase serviceImpl;
    private final int methodId;

    public MethodHandlers(DFilterImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_BIDDERS:
          serviceImpl.getBidders((com.vdopia.dfilter.api.grpc.DFilterInput) request,
              (io.grpc.stub.StreamObserver<com.vdopia.dfilter.api.grpc.BidderSelected>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    return new io.grpc.ServiceDescriptor(SERVICE_NAME,
        METHOD_GET_BIDDERS);
  }

}
