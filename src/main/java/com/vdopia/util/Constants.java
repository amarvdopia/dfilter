package com.vdopia.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constants {

	public static final String AWS_S3_DFILTER_ACCESS_KEY = "dfilterserver.aws_s3_dfilter_access_key";
	public static final String AWS_S3_DFILTER_SECRET_KEY = "dfilterserver.aws_s3_dfilter_secret_key";

	public static final String AWS_DFILTER_S3DUMPBUCKET = "dfilterserver.dFilterS3DumpKey";
	public static final String DFILTER_FILEPATH = "dfilterserver.filestorepath";
	public static final String AWS_DFILTER_CACHE_FILENAME = "dfilterserver.cachefilename";
	public static final String DFILTER_GRPC_SERVER_PORT = "dfilter_grpc_server_port";

	public static int DFILTER_GPRC_SERVER_PORT_INT = -1;

	static {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			String filename = "dfilter.properties";
			input = Constants.class.getClassLoader().getResourceAsStream(filename);
			if(input==null){
				System.out.println("Error, unable to find " + filename);
			}
			prop.load(input);
			String portStr = prop.getProperty(Constants.DFILTER_GRPC_SERVER_PORT);               
			DFILTER_GPRC_SERVER_PORT_INT = Integer.valueOf(portStr);
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (NumberFormatException nfe){
			nfe.printStackTrace();
		} 
		finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
