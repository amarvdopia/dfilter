package com.vdopia.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VdopiaFileUtils {

	private static final Logger logger = LoggerFactory.getLogger(VdopiaFileUtils.class.getName());

	/** Gzip the contents of the from file and save in the to file. */
	public static boolean gzipFile(String from, String to) {
		FileInputStream in = null;
		GZIPOutputStream out = null;
		boolean result = false;
		try {
			in = new FileInputStream(from);
			out = new GZIPOutputStream(new FileOutputStream(to));
			byte[] buffer = new byte[4096];
			int bytes_read;
			while ((bytes_read = in.read(buffer)) != -1) {
				out.write(buffer, 0, bytes_read);
			}

			result = true;

		} catch (IOException e) {
			logger.error("IOException ", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
		}

		return result;
	}

	/** Zip the contents of the directory, and save it in the zipfile */
	public static boolean zipDirectory(String dir, String zipfile) {
		boolean response = false;
		ZipOutputStream out = null;
		try {
			File d = new File(dir);
			if (!d.isDirectory())
				throw new IllegalArgumentException("Compress: not a directory:  " + dir);
			String[] entries = d.list();
			byte[] buffer = new byte[4096];
			int bytes_read;

			out = new ZipOutputStream(new FileOutputStream(zipfile));

			for (int i = 0; i < entries.length; i++) {
				File f = new File(d, entries[i]);
				if (f.isDirectory())
					continue;
				FileInputStream in = new FileInputStream(f);

				ZipEntry entry = new ZipEntry(f.getName());
				out.putNextEntry(entry);
				while ((bytes_read = in.read(buffer)) != -1)
					out.write(buffer, 0, bytes_read);
				in.close();
			}
			response = true;

		} catch (IOException | IllegalArgumentException e) {
			logger.error("IOException ", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
		}
		return response;

	}

	/**
	 * Unzip it
	 */
	public static void unZipIt(String zipFilePath, String outputFolder)  throws IOException {

		byte[] buffer = new byte[1024];

		ZipInputStream zis = null;
		ZipEntry ze = null;
		FileOutputStream fos = null;
		try {

			// create output directory is not exists
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}

			// get the zip file content
			zis = new ZipInputStream(new FileInputStream(zipFilePath));
			// get the zipped file list entry
			ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

		} catch (Exception ex) {
			logger.error("Error: {}", ex);
			throw new IOException(ex);
		} finally {
			if (null != zis) {
				try {
					zis.closeEntry();
					zis.close();
				} catch (Exception e) {
				}
			}

			if (null != fos) {
				try {
					fos.close();
				} catch (Exception e) {
				}
			}
		}
	}
	
	public static boolean zipFile(File file, String zipFileName) {
		
		File zipFile = new File(zipFileName);
		if(zipFile.exists()){
			logger.info("File Already zipped so deleting the file ");
			zipFile.delete();
		}
		
		byte[] buffer = new byte[1024];
		try{

    		FileOutputStream fos = new FileOutputStream(zipFileName);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(file.getName());
    		zos.putNextEntry(ze);
    		FileInputStream in = new FileInputStream(file.getPath());

    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();

    		//remember close it
    		zos.close();

    		return true;

    	}catch(IOException ex){
    	   logger.error("Error: {}", ex);
    	}
		return false;
	}

	
}
