package com.vdopia.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VersionNumber {

	public VersionNumber() {

	}

	private static Properties props = new Properties();

	private static final Logger logger = LoggerFactory.getLogger(VersionNumber.class.getName());

	static {

		try (InputStream inputStream = VersionNumber.class.getClassLoader()
				.getResourceAsStream("version.properties");) {
			props.load(inputStream);
		} catch (IOException e) {
			logger.error("Exception occured in loading version properties", e.getMessage());
		}
	}

	public static String getProperty(String key) {
		if (VersionNumber.props != null)
			return VersionNumber.props.getProperty(key);
		else
			return null;
	}

	public static boolean isUrlForVersion(String uri) {
		String finalUri = uri;
		return finalUri.toLowerCase().contains("/version");
	}

}
