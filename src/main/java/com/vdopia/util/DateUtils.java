package com.vdopia.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

	public static TimeZone timeZone = TimeZone.getTimeZone("UTC");

	public static String getLastNTime(int n, Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/h");
		return dateFormat.format(date.getTime() - n * 60 * 60);
	}

	public static String getTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		sdf.setTimeZone(timeZone);
		String timeStamp = sdf.format(date);
		return timeStamp;
	}

	public static String getTime(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		sdf.setTimeZone(timeZone);
		String timeStamp = sdf.format(date);
		return timeStamp;
	}

}
