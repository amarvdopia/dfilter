package com.vdopia.cache;

import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.cache.DFilterCacheBlockSegregatorByQuarter;
import com.vdopia.dfilter.model.DFilterPerformanceValue;

/**
 * This is the class for storing cache objects for dfilter
 * 
 * @author amar
 *
 */

public class DFilterCache {

	private static final Logger logger = LoggerFactory.getLogger(DFilterCache.class);

	// TODO : Update code to use DFilterCacheBlock instead of
		// DFilterCacheBlockSegregatorByQuarter

		// TODO : Make dFilterMap private . Made public for debugging
		public static Map<DFilterCacheBlockSegregatorByQuarter, Map<String, DFilterPerformanceValue>> dFilterMap;
		// private static Map<DFilterCacheBlock, Map<String, Float>> dFilterMap;

		private DFilterCache() {
			dFilterMap = new ConcurrentHashMap<DFilterCacheBlockSegregatorByQuarter, Map<String, DFilterPerformanceValue>>(25);
		}

		private static DFilterCache instance = null;

		public static DFilterCache getInstance() {
			if (instance == null) {
				synchronized (DFilterCache.class) {
					if (instance == null) {
						instance = new DFilterCache();
					}
				}
			}
			return instance;
		}

		public Map<String, DFilterPerformanceValue> getCurrentQuarterDFilterCache() {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int date = cal.get(Calendar.DAY_OF_MONTH);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int quarter = (cal.get(Calendar.MINUTE) / 15) + 1;

			DFilterCacheBlockSegregatorByQuarter cacheBlock = new DFilterCacheBlockSegregatorByQuarter(year, month, date,
					hour, quarter);
			/*
			 * logger.info(
			 * "Inside DfilterCache : CurrentcacheBlock requested for dfilter : " +
			 * cacheBlock);
			 * 
			 * logger.info("Inside DfilterCache : DFilterCache has " +
			 * this.dFilterMap.size() + " entries  : " +
			 * Arrays.toString(this.dFilterMap.keySet().toArray()));
			 */
			Map<String, DFilterPerformanceValue> dFilterCacheBlockMap = this.dFilterMap.get(cacheBlock);
			/*
			 * if (dFilterCacheBlockMap != null) { logger.info(
			 * "Inside DfilterCache : cache retrieved has size : " +
			 * dFilterCacheBlockMap.size()); } else { logger.info(
			 * "Inside DfilterCache : cache retrieved is null"); }
			 */

			if (dFilterCacheBlockMap == null) {
				Map<String, DFilterPerformanceValue> newDFilterMap = new ConcurrentHashMap<String, DFilterPerformanceValue>();
				this.dFilterMap.put(cacheBlock, newDFilterMap);
				dFilterCacheBlockMap = this.dFilterMap.get(cacheBlock);
			}
			return dFilterCacheBlockMap;
		}

		public Map<String, DFilterPerformanceValue> getNextQuarterDFilterCache() {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			cal.add(Calendar.MINUTE, 15);

			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int date = cal.get(Calendar.DAY_OF_MONTH);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int quarter = (cal.get(Calendar.MINUTE) / 15) + 1;

			DFilterCacheBlockSegregatorByQuarter cacheBlock = new DFilterCacheBlockSegregatorByQuarter(year, month, date,
					hour, quarter);
			Map<String, DFilterPerformanceValue> dFilterCacheBlockMap = this.dFilterMap.get(cacheBlock);
			if (dFilterCacheBlockMap == null) {
				Map<String, DFilterPerformanceValue> newDFilterMap = new ConcurrentHashMap<String, DFilterPerformanceValue>();
				this.dFilterMap.put(cacheBlock, newDFilterMap);
				dFilterCacheBlockMap = this.dFilterMap.get(cacheBlock);
			}
			return dFilterCacheBlockMap;
		}

		public Map<String, DFilterPerformanceValue> getDFilterCache(DFilterCacheBlockSegregatorByQuarter cacheBlock) {
			Map<String, DFilterPerformanceValue> dFilterCacheBlockMap = dFilterMap.get(cacheBlock);
			if (dFilterCacheBlockMap == null) {
				Map<String, DFilterPerformanceValue> newDFilterMap = new ConcurrentHashMap<String, DFilterPerformanceValue>();
				dFilterMap.put(cacheBlock, newDFilterMap);
				dFilterCacheBlockMap = dFilterMap.get(cacheBlock);
			}
			return dFilterCacheBlockMap;
		}

		public void cleanOldDFilterCache(int timeThresholdInMillis) {
			Calendar curr_dateTimeCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			Set<Entry<DFilterCacheBlockSegregatorByQuarter, Map<String, DFilterPerformanceValue>>> mapEntrySet = this.dFilterMap.entrySet();

			for (Entry<DFilterCacheBlockSegregatorByQuarter, Map<String, DFilterPerformanceValue>> entry : mapEntrySet) {
				DFilterCacheBlockSegregatorByQuarter dFilterCacheBlockKey = entry.getKey();
				Calendar calendarForKey = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				calendarForKey.set(dFilterCacheBlockKey.getYear(), dFilterCacheBlockKey.getMonth(),
						dFilterCacheBlockKey.getDate(), dFilterCacheBlockKey.getHour(),
						(dFilterCacheBlockKey.getQuarter() * 15 - 1), 0);
				if ((curr_dateTimeCal.getTimeInMillis() - calendarForKey.getTimeInMillis()) > timeThresholdInMillis) {
					dFilterMap.remove(dFilterCacheBlockKey);
				}
			}

		}

	}
