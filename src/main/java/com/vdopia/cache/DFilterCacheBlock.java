package com.vdopia.cache;

/**
 * Interface for cache block for dfilter. Cache block to be used as key in DfilterCache
 * 
 * @author amar
 *
 */

public interface DFilterCacheBlock {
	
	public DFilterCacheBlock getNextCacheBlock();

	public DFilterCacheBlock getCurrentCacheBlock();

	public int hashCode();
	
	public boolean equals(Object obj);
	
	public String toString();
	

}
