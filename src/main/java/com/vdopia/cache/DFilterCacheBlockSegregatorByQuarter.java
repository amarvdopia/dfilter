package com.vdopia.cache;

import java.util.Calendar;
import java.util.TimeZone;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Cache block object segregated by quarter (15 minutes). This will be used as key in DfilterCache
 * 
 * @author amar
 *
 */

public class DFilterCacheBlockSegregatorByQuarter implements DFilterCacheBlock {

	private int year;
	private int month;
	private int date;
	private int hour;
	private int quarter;

	private DFilterCacheBlockSegregatorByQuarter() {

	}

	public static DFilterCacheBlockSegregatorByQuarter getInstance() {
		DFilterCacheBlockSegregatorByQuarter cacheBlock = new DFilterCacheBlockSegregatorByQuarter()
				.getCurrentCacheBlock();
		return cacheBlock;
	}

	public DFilterCacheBlockSegregatorByQuarter(int year, int month, int date, int hour, int quarter) {
		this.year = year;
		this.month = month;
		this.date = date;
		this.hour = hour;
		this.quarter = quarter;
	}

	public DFilterCacheBlockSegregatorByQuarter getNextCacheBlock() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.add(Calendar.MINUTE, +15);

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int quarter = (cal.get(Calendar.MINUTE) / 15) + 1;

		DFilterCacheBlockSegregatorByQuarter cacheBlock = new DFilterCacheBlockSegregatorByQuarter(year, month, date,
				hour, quarter);
		return cacheBlock;
	}

	public DFilterCacheBlockSegregatorByQuarter getCurrentCacheBlock() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int quarter = (cal.get(Calendar.MINUTE) / 15) + 1;

		DFilterCacheBlockSegregatorByQuarter cacheBlock = new DFilterCacheBlockSegregatorByQuarter(year, month, date,
				hour, quarter);
		return cacheBlock;
	}

	@Override
	public String toString() {
		String strVal = this.getYear() + "_" + String.format("%02d", this.getMonth()) + "_"
				+ String.format("%02d", this.getDate()) + "_" + String.format("%02d", this.getHour()) + "_"
				+ this.getQuarter();
		return strVal;
	}

	@Override
	public int hashCode() {
		int hashCode = new HashCodeBuilder(17, 31).append(year).append(month).append(date).append(hour).append(quarter)
				.toHashCode();
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DFilterCacheBlockSegregatorByQuarter)) {
			return false;
		}
		if (obj == this) {
			return true;
		}

		DFilterCacheBlockSegregatorByQuarter cacheBlock = (DFilterCacheBlockSegregatorByQuarter) obj;
		boolean is_equal = new EqualsBuilder().append(year, cacheBlock.year).append(month, cacheBlock.month)
				.append(date, cacheBlock.date).append(hour, cacheBlock.hour).append(quarter, cacheBlock.quarter)
				.isEquals();
		return is_equal;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDate() {
		return date;
	}

	public int getHour() {
		return hour;
	}

	public int getQuarter() {
		return quarter;
	}

}
