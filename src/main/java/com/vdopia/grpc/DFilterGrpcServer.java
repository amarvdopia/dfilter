//DFilterGrpcServer.java
package com.vdopia.grpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.vdopia.dfilter.api.grpc.DFilterGrpc;
import com.vdopia.dfilter.api.grpc.BidderSelected;
import com.vdopia.dfilter.api.grpc.DFilterInput;
import com.vdopia.dfilter.DFilterHandler;
import com.vdopia.util.Constants;


/**
 * Server that manages startup/shutdown of a {@code DFilterGrpc} server.
 */
public class DFilterGrpcServer {
	private static final Logger logger = Logger.getLogger(DFilterGrpcServer.class.getName());

	private Server server;
  private static int port = Constants.DFILTER_GPRC_SERVER_PORT_INT;

	private void start() throws IOException {
    
		server = ServerBuilder.forPort(port)
		.addService(new DFilterImpl())
		.build()
		.start();
		logger.info("Server started, listening on " + port);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
        // Use stderr here since the logger may have been reset by its JVM shutdown hook.
				System.err.println("*** shutting down gRPC server since JVM is shutting down");
				DFilterGrpcServer.this.stop();
				System.err.println("dFilter server shut down");
			}
		});
	}

  private void init(){
    if (port < 0){
      System.exit(-1);
    }
  }

	private void stop() {
		if (server != null) {
			server.shutdown();
		}
	}

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
  	if (server != null) {
  		server.awaitTermination();
  	}
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
  	final DFilterGrpcServer server = new DFilterGrpcServer();
  	server.init();
  	server.start();
  	server.blockUntilShutdown();
  }

  private class DFilterImpl extends DFilterGrpc.DFilterImplBase {

  	@Override
  	public void getBidders(DFilterInput req, StreamObserver<BidderSelected> responseObserver) {
      //call handler to get bidders
      List<String> bidderList = new ArrayList<String>();
      //bidderList = DFilterHandler.getBidders(req);

      //below should be commented out once handler is wired up.
      bidderList.add("bidder1");
      bidderList.add("bidder2");


  		BidderSelected reply = BidderSelected.newBuilder().addAllBidder(bidderList).build();
  		responseObserver.onNext(reply);
  		responseObserver.onCompleted();
  	}
  }
}