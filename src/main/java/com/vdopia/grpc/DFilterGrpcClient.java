//DFilterClient.java


package com.vdopia.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.vdopia.dfilter.api.grpc.DFilterGrpc;
import com.vdopia.dfilter.api.grpc.DFilterInput;
import com.vdopia.dfilter.api.grpc.BidderSelected;
import com.vdopia.dfilter.api.grpc.DFilterInput.DeviceType;
import com.vdopia.util.Constants;

/**
 * A simple client that requests a greeting from the {@link DFilterServer}.
 */
public class DFilterGrpcClient {
	private static final Logger logger = Logger.getLogger(DFilterGrpcClient.class.getName());

	private final ManagedChannel channel;
	private final DFilterGrpc.DFilterBlockingStub blockingStub;

	/** Construct client connecting to DFilter server at {@code host:port}. */
	public DFilterGrpcClient(String host, int port) {
		channel = ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
		.usePlaintext(true)
		.build();
		blockingStub = DFilterGrpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/** Say hello to server. */
	public void getBidders(List<String> bidderIds,
		String apiKey,
		String country,
		String propertyName,
		String locationCode,
		DeviceType deviceType,
		boolean hasGPSLocation,
		boolean hasBidderCookie,
		boolean hasIdForAdvertiser) {
		logger.info("Will try to getBidders " + " ...");
		DFilterInput request = DFilterInput.newBuilder()
		.addAllBidderId(bidderIds)
		.setApiKey(apiKey)
		.setCountry(country)
		.setPropertyName(propertyName)
		.setLocationCode(locationCode)
		.setDeviceType(deviceType)
		.setHasGPSLocation(hasGPSLocation)
		.setHasBidderCookie(hasBidderCookie)
		.setHasIDForAdvertiser(hasIdForAdvertiser)
		.build();
		BidderSelected response;
		try {
			response = blockingStub.getBidders(request);
		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			return;
		}
		logger.info("BidderSelected by dFilter: " + response.getBidderCount());
	}

  /**
   * Greet server. If provided, the first element of {@code args} is the name to use in the
   * greeting.
   */
  public static void main(String[] args) throws Exception {
  	int port = Constants.DFILTER_GPRC_SERVER_PORT_INT;
  	DFilterGrpcClient client = new DFilterGrpcClient("localhost", port);
  	try {
  		/* Access a service running on the local machine on port 50051 */
  		List<String> bidderIds = new ArrayList<String>();
  		bidderIds.add("bidder1");
  		String apiKey = "apiKey1";
  		String country = "USA";
  		String propertyName = "abc.com";
  		String locationCode = "345";
  		DeviceType deviceType = DeviceType.IOS;
  		boolean hasGPSLocation = true;
  		boolean hasBidderCookie = false;
  		boolean hasIdForAdvertiser = true;
  		client.getBidders(bidderIds,apiKey,country,propertyName,locationCode,deviceType,hasGPSLocation,hasBidderCookie,hasIdForAdvertiser);
  	}
  	finally {
  		client.shutdown();
  	}
  }
}
