package com.vdopia.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HealthHandler implements Runnable{
	
	private static final Logger logger = LoggerFactory.getLogger(HealthHandler.class);

	@Override
	public void run() {
		

		if (!HealthCheck.isTrafficOn()) {
			HealthCheck.setCurrentHealthStat(HealthState.FAIL);
		    logger.warn("Traffic is set to off....");
			return;
		}
		
		HealthState currentState = HealthState.PASS;
		HealthChecker healthChecker = new PassHandler();
		
		while (!currentState.equals(HealthState.PASS)) {
			try {
				currentState = healthChecker.checkHealth();
				HealthCheck.setCurrentHealthStat(currentState);
				healthChecker = healthChecker.nextHealthChecker();
				logger.trace("Next health checker : {}",currentState);
			} catch (HealthCheckException e) {
				logger.warn("HealthCheck not working....");
				currentState = HealthState.FAIL;
				currentState.setMessage(e.getHealthCheckStatus().getError());
				HealthCheck.setCurrentHealthStat(currentState);
				return;
			}
		}
	}
	
}
