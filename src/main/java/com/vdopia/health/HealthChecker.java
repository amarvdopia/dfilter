package com.vdopia.health;

public interface HealthChecker {
	
	public HealthState checkHealth() throws HealthCheckException;
	public HealthChecker nextHealthChecker();

}
