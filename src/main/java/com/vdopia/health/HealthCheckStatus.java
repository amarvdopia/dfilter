package com.vdopia.health;

public class HealthCheckStatus {

	private final boolean data;
	private final String error;

	public HealthCheckStatus(boolean data, String error) {
		super();
		this.data = data;
		this.error = error;
	}

	/**
	 * @return the data
	 */
	public boolean isData() {
		return data;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

}
