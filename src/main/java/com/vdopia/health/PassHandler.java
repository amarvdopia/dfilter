package com.vdopia.health;

public class PassHandler implements HealthChecker {

	@Override
	public HealthState checkHealth() throws HealthCheckException {
		return HealthState.PASS;
	}

	@Override
	public HealthChecker nextHealthChecker() {
		return null;
	}

}
