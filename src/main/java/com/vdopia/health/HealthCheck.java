package com.vdopia.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HealthCheck {

	private static final Logger logger = LoggerFactory.getLogger(HealthCheck.class.getName());
	private static boolean httpClientOn = true;
	private static boolean trafficOn = true;

	private static HealthState currentHealthStat = HealthState.PASS;

	public enum MysqlSlaveFail {
		memcache, mysql, pass
	}
	
	/*
	 * @return the trafficOn
	 */
	public static boolean isHttpClientOn() {
		return httpClientOn;
	}

	/**
	 * @param trafficOn
	 *            the trafficOn to set
	 */
	public static void setHttpClientOn(boolean httpClientOn) {
		HealthCheck.httpClientOn = httpClientOn;
	}
	
	/**
	 * @return the currentHealthStat
	 */
	public static HealthState getCurrentHealthStat() {
		return currentHealthStat;
	}
	
	/**
	 * @param currentHealthStat the currentHealthStat to set
	 */
	public static void setCurrentHealthStat(HealthState currentHealthStat) {
		HealthCheck.currentHealthStat = currentHealthStat;
	}
	
	/*
	 * @return the trafficOn
	 */
	public static boolean isTrafficOn() {
		return trafficOn;
	}

	/**
	 * @param trafficOn
	 *            the trafficOn to set
	 */
	public static void setTrafficOn(boolean trafficOn) {
		HealthCheck.trafficOn = trafficOn;
	}

}
