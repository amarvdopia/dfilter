package com.vdopia.health;

import com.vdopia.health.HealthCheckStatus;

public class HealthCheckException extends Exception {

	private static final long serialVersionUID = 3231434789367735322L;
	private HealthCheckStatus checkInfo = null;
	
	/*
	 * Creates a new instance
	 */
	public HealthCheckException() {
		super();
	}

	/*
	 * Creates a new instance
	 */
	public HealthCheckException(HealthCheckStatus checkInfo) {
		super(checkInfo.getError());
		this.checkInfo = checkInfo;
	}

	/*
	 * Creates a new instance
	 */
	public HealthCheckException(Throwable cause) {
		super(cause);
	}

	/*
	 * Creates a new instance
	 */
	public HealthCheckException(String message, Throwable cause) {
		super(message);
	}

	public HealthCheckStatus getHealthCheckStatus() {
		return checkInfo;
	}

	public void setHealthCheckStatus(HealthCheckStatus checkInfo) {
		this.checkInfo = checkInfo;
	}

	
}
