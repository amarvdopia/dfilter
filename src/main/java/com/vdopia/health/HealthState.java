package com.vdopia.health;

public enum HealthState {

	PASS("RETURN OK"),
	FAIL("FAIL");
	
	private String message;
	
	private HealthState(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
