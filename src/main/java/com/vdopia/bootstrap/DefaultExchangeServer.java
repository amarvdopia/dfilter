package com.vdopia.bootstrap;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.health.HealthHandler;

import io.netty.bootstrap.ChannelFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.ServerChannel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import io.netty.util.concurrent.GlobalEventExecutor;

public class DefaultExchangeServer implements ExchangeServer {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultExchangeServer.class);

	private final TransportProtocol transportProtocol;
	/*
	 * The address that the server will attempt to bind to.
	 */
	private final InetSocketAddress requestedAddress;
	/*
	 * The actual address to which the server is bound. May be different from
	 * the requestedAddress in some circumstances, for example when the
	 * requested port is 0.
	 */
	private volatile InetSocketAddress localAddress;
	private volatile InetSocketAddress boundAddress;
	private final ObjectStoreFactory objectStoreFactory;
	private final FiltersSource filtersSource;
	private final boolean transparent;

	private volatile int connectTimeout;
	private volatile int idleConnectionTimeout;
	private volatile GlobalTrafficShapingHandler globalTrafficShapingHandler;

	/**
	 * True when the proxy has already been stopped by calling {@link #stop()}
	 * or {@link #abort()}.
	 */
	private final AtomicBoolean stopped = new AtomicBoolean(false);

	/**
	 * Track all ActivityTrackers for tracking proxying activity.
	 */
	private final Collection<ActivityTracker> activityTrackers = new ConcurrentLinkedQueue<ActivityTracker>();

	/**
	 * Keep track of all channels created by this proxy server for later
	 * shutdown when the proxy is stopped.
	 */
	private final ChannelGroup allChannels = new DefaultChannelGroup("Object-Server", GlobalEventExecutor.INSTANCE);

	/**
	 * JVM shutdown hook to shutdown this proxy server. Declared as a
	 * class-level variable to allow removing the shutdown hook when the proxy
	 * server is stopped normally.
	 */
	private final Thread jvmShutdownHook = new Thread(new Runnable() {

		public void run() {
			abort();
		}

	}, "ChocolateServer-shutdown-hook");

	private final HealthHandler healthHandler;
	private final ServiceType serviceType;

	public DefaultExchangeServer(TransportProtocol transportProtocol, InetSocketAddress requestedAddress,
			ObjectStoreFactory objectStoreFactory, HealthHandler healthHandler, ServiceType serviceType,
			FiltersSource filtersSource, boolean transparent, int idleConnectionTimeout,
			Collection<ActivityTracker> activityTrackers, int connectTimeout, InetSocketAddress localAddress) {

		this.transportProtocol = transportProtocol;
		this.requestedAddress = requestedAddress;
		this.objectStoreFactory = objectStoreFactory;
		this.filtersSource = filtersSource;
		this.transparent = transparent;
		this.idleConnectionTimeout = idleConnectionTimeout;
		if (activityTrackers != null) {
			this.activityTrackers.addAll(activityTrackers);
		}
		this.connectTimeout = connectTimeout;
		this.localAddress = localAddress;
		this.healthHandler = healthHandler;
		this.serviceType = serviceType;

	}

	public int getIdleConnectionTimeout() {
		return this.idleConnectionTimeout;
	}

	public void setIdleConnectionTimeout(int idleConnectionTimeout) {
		this.idleConnectionTimeout = idleConnectionTimeout;
	}

	public int getConnectTimeout() {
		return this.connectTimeout;
	}

	public void setConnectTimeout(int connectTimeoutMs) {
		this.connectTimeout = connectTimeoutMs;
	}

	public void stop() {
		doStop(true);
	}

	public void abort() {
		doStop(false);
	}

	public InetSocketAddress getListenAddress() {
		return this.boundAddress;
	}

	public void setThrottle(long readThrottleBytesPerSecond, long writeThrottleBytesPerSecond) {
		// TODO Auto-generated method stub

	}

	protected void doStop(boolean graceful) {
		// only stop the server if it hasn't already been stopped
		if (stopped.compareAndSet(false, true)) {
			if (graceful) {
				LOG.info("Shutting down proxy server gracefully");
			} else {
				LOG.info("Shutting down proxy server immediately (non-graceful)");
			}

			closeAllChannels(graceful);

			// remove the shutdown hook that was added when the proxy was
			// started, since it has now been stopped
			try {
				Runtime.getRuntime().removeShutdownHook(jvmShutdownHook);
			} catch (IllegalStateException e) {
				// ignore -- IllegalStateException means the VM is already
				// shutting down
			}

			LOG.info("Done shutting down proxy server");
		}
	}

	/**
	 * Closes all channels opened by this proxy server.
	 *
	 * @param graceful
	 *            when false, attempts to shutdown all channels immediately and
	 *            ignores any channel-closing exceptions
	 */
	protected void closeAllChannels(boolean graceful) {
		LOG.info("Closing all channels " + (graceful ? "(graceful)" : "(non-graceful)"));

		ChannelGroupFuture future = allChannels.close();

		// if this is a graceful shutdown, log any channel closing failures. if
		// this isn't a graceful shutdown, ignore them.
		if (graceful) {
			try {
				future.await(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();

				LOG.warn("Interrupted while waiting for channels to shut down gracefully.");
			}

			if (!future.isSuccess()) {
				for (ChannelFuture cf : future) {
					if (!cf.isSuccess()) {
						LOG.info("Unable to close channel.  Cause of failure for {} is {}", cf.channel(), cf.cause());
					}
				}
			}
		}
	}

	public ExchangeServer start() {
		if (!isStopped()) {
			LOG.info("Starting proxy at address: " + this.requestedAddress);
			try {
				doStart();
			} catch (Exception e) {
				LOG.error("Error while initializing ", e);
			}
		} else {
			throw new IllegalStateException("Attempted to start proxy, but proxy's server group is already stopped");
		}

		return this;
	}

	public boolean isStopped() {
		return stopped.get();
	}

	private void doStart() {

		EventLoopGroup bossGroup = new NioEventLoopGroup(); // (1)
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		ServerBootstrap serverBootstrap = new ServerBootstrap().group(bossGroup, workerGroup);

		int backlog = 1024;
		backlog = Configuration.getConfig().getInt("bootstrap.backlog");
		serverBootstrap.option(ChannelOption.SO_BACKLOG, backlog);

		boolean keepAlive = true;
		keepAlive = Configuration.getConfig().getBoolean("bootstrap.keepAlive");
		serverBootstrap.option(ChannelOption.SO_KEEPALIVE, keepAlive);

		int connectTimeout = 10000;
		connectTimeout = Configuration.getConfig().getInt("bootstrap.connectTimeoutMillis");
		serverBootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeout);

		boolean reuseAddress = true;
		reuseAddress = Configuration.getConfig().getBoolean("bootstrap.reuseAddress");
		serverBootstrap.option(ChannelOption.SO_REUSEADDR, reuseAddress);

		// if the server is sending 1000 messages per sec, optimum write buffer
		// water marks will
		// prevent unnecessary throttling, Check NioSocketChannelConfig doc
		int writeBufferLowWaterMark = 32768;
		writeBufferLowWaterMark = Configuration.getConfig().getInt("bootstrap.writeBufferLowWaterMark");
		serverBootstrap.childOption(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, writeBufferLowWaterMark);

		int writeBufferHighWaterMark = 65536;
		writeBufferHighWaterMark = Configuration.getConfig().getInt("bootstrap.writeBufferHighWaterMark");
		serverBootstrap.childOption(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, writeBufferHighWaterMark);

		ChannelInitializer<Channel> initializer = new ChannelInitializer<Channel>() {
			protected void initChannel(Channel ch) throws Exception {
				new HttpRequestService(DefaultExchangeServer.this.getHealthHandler(), DefaultExchangeServer.this,
						ch.pipeline());
			};
		};

		switch (transportProtocol) {
		case TCP:
			LOG.info("Proxy listening with TCP transport");
			serverBootstrap.channelFactory(new ChannelFactory<ServerChannel>() {
				public ServerChannel newChannel() {
					return new NioServerSocketChannel();
				}
			});
			break;
		default:
			throw new UnknownTransportProtocolException(transportProtocol);
		}
		serverBootstrap.childHandler(initializer);
		ChannelFuture future = serverBootstrap.bind(requestedAddress).addListener(new ChannelFutureListener() {

			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess()) {
					registerChannel(future.channel());
				}

			}

		}).awaitUninterruptibly();

		Throwable cause = future.cause();
		if (cause != null) {
			throw new RuntimeException(cause);
		}

		this.boundAddress = ((InetSocketAddress) future.channel().localAddress());
		LOG.info("Proxy started at address: " + this.boundAddress);

		Runtime.getRuntime().addShutdownHook(jvmShutdownHook);
	}

	protected void unregisterChannel(Channel channel) {
		allChannels.remove(channel);
	}

	protected void registerChannel(Channel channel) {
		allChannels.add(channel);
	}

	public GlobalTrafficShapingHandler getGlobalTrafficShapingHandler() {
		return globalTrafficShapingHandler;
	}

	public void setGlobalTrafficShapingHandler(GlobalTrafficShapingHandler globalTrafficShapingHandler) {
		this.globalTrafficShapingHandler = globalTrafficShapingHandler;
	}

	public ObjectStoreFactory getObjectStoreFactory() {
		return this.objectStoreFactory;
	}

	public FiltersSource getFiltersSource() {
		return filtersSource;
	}

	public Collection<ActivityTracker> getActivityTrackers() {
		return activityTrackers;
	}

	public HealthHandler getHealthHandler() {
		return healthHandler;
	}

}
