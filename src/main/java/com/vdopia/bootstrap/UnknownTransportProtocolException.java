package com.vdopia.bootstrap;

public class UnknownTransportProtocolException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UnknownTransportProtocolException(TransportProtocol transportProtocol) {
        super(String.format("Unknown TransportProtocol: %1$s", transportProtocol));
    }
}

