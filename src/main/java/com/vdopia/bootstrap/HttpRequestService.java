package com.vdopia.bootstrap;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.health.HealthHandler;
import com.vdopia.processors.ConfigureProcessor;
import com.vdopia.processors.DFilterProcessor;
import com.vdopia.processors.VersionProcessor;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.concurrent.Promise;

@Sharable
public class HttpRequestService extends SimpleChannelInboundHandler<FullHttpRequest>{

	private static final Logger logger = LoggerFactory.getLogger(HttpRequestService.class);

	/**
	 * Variables for Netty Handlers
	 */
	protected DefaultExchangeServer exchangeServer;
	protected volatile ChannelHandlerContext ctx;
	protected volatile Channel channel;
	private WriteHandler writeHandler;
	private HealthHandler healthHandler;
	protected volatile long lastReadTime = 0;
	
	public HttpRequestService(HealthHandler healthHandler,DefaultExchangeServer defaultExchangeServer, ChannelPipeline pipeline) {
		this.exchangeServer = defaultExchangeServer;
		this.healthHandler = healthHandler;
		initChannelPipeline(pipeline);
	}

	public void initChannelPipeline(ChannelPipeline pipeline) {
		logger.info("Configuring ChannelPipeline");
		
		// We want to allow longer request lines, headers, and chunks
		// respectively.
		pipeline.addLast("decoder", new HttpRequestDecoder(8192, 8192 * 2, 8192 * 2));
		pipeline.addLast("encoder", new HttpResponseEncoder());
		pipeline.addLast("httpObjectAggregator", new HttpObjectAggregator(1048576));
		pipeline.addLast("idle", new IdleStateHandler(0, 0, exchangeServer.getIdleConnectionTimeout()));
		pipeline.addLast("handler", this);
		pipeline.addLast("exceptionHandlerName", new ExceptionHandler());

	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
		logger.info("Reading: {}", msg);
		lastReadTime = System.currentTimeMillis();
		readHTTPInitial(msg);
	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		try {
			this.ctx = ctx;
			this.channel = ctx.channel();
			this.writeHandler = new WriteHandler(this.channel);
			this.exchangeServer.registerChannel(ctx.channel());
		} finally {
			super.channelRegistered(ctx);
		}
	}

	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
		try {
			this.ctx = ctx;
			this.channel = ctx.channel();
			this.exchangeServer.unregisterChannel(channel);
		} finally {
			super.channelUnregistered(ctx);
		}
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		try {
			disconnect();
		} finally {
			super.channelInactive(ctx);
		}
	}

	private Future<Void> disconnect() {
		if (channel == null) {
			return null;
		} else {
			final Promise<Void> promise = channel.newPromise();
			this.writeHandler.writeToChannel(Unpooled.EMPTY_BUFFER)
					.addListener(new GenericFutureListener<Future<? super Void>>() {
						public void operationComplete(Future<? super Void> future) throws Exception {
							closeChannel(promise);
						}
					});
			return promise;
		}
	}

	private void closeChannel(final Promise<Void> promise) {
		channel.close().addListener(new GenericFutureListener<Future<? super Void>>() {
			public void operationComplete(Future<? super Void> future) throws Exception {
				if (future.isSuccess()) {
					promise.setSuccess(null);
				} else {
					promise.setFailure(future.cause());
				}
			};
		});
	}

	private void readHTTPInitial(FullHttpRequest httpRequest) {

		logger.debug("Got request: {}", httpRequest);
		logger.info("Request received by hudson");

		String uri = httpRequest.getUri();
		QueryStringDecoder queryStringDecoder = new QueryStringDecoder(uri);
		Map<String, String> queryParameters = queryStringDecoder.getParameters();

		ObjectStoreFactory objectStoreFactory = exchangeServer.getObjectStoreFactory();

		HttpMethod method = httpRequest.getMethod();
		if (method == HttpMethod.GET) {
			RequestType requestType = RequestType.getCallType(queryStringDecoder.getPath());
			
			logger.info("requestType {}",requestType.name());
			switch (requestType) {
			case DFILTER:
				ByteBuf content = httpRequest.content();
				String payLoad;
				if (content.isReadable()) {
					payLoad = content.toString(CharsetUtil.UTF_8);
					DFilterProcessor dprocessor = new DFilterProcessor(objectStoreFactory);
					dprocessor.process(payLoad,queryParameters, this.writeHandler);
				}else{
					writeHandler.writeJson(null);
				}
				
				break;
			case VERSION:
				VersionProcessor verprocess = new VersionProcessor();
				verprocess.process(queryParameters, this.writeHandler);
				break;
			case CONFIGURE:
				ConfigureProcessor cprocess = new ConfigureProcessor();
				cprocess.process(queryParameters, this.writeHandler);
				break;
			default:
				writeHandler.writeJson(null);
				break;
			}
		}
	}

}
