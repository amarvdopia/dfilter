package com.vdopia.bootstrap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.ReferenceCounted;

public class WriteHandler {

	private static final Logger LOG = LoggerFactory.getLogger(WriteHandler.class.getName());
	private final Channel channel;

	private static final CharSequence X_CACHE_VALUE = HttpHeaders
			.newEntity("no-store, no-cache, must-revalidate, post-check=0, pre-check=0");

	private static final CharSequence X_P3P_VALUE = HttpHeaders.newEntity(
			"policyref=\"http://www.vdopia.com/w3c/p3p.xml\", CP=\"NOI DSP COR CURa ADMa DEVa PSAo PSDo OUR BUS UNI COM NAV STA\"");

	private static final CharSequence X_ALLOW_CONTROL_VALUE = HttpHeaders.newEntity("*");

	public WriteHandler(Channel channel) {
		this.channel = channel;
	}

	public void writeEmptyBuffer() {
		write(Unpooled.EMPTY_BUFFER);
	}

	public void write(Object msg) {
		if (msg instanceof ReferenceCounted) {
			LOG.debug("Retaining reference counted message");
			((ReferenceCounted) msg).retain();
		}

		doWrite(msg);
	}

	void doWrite(Object msg) {
		LOG.info("Writing: {}", msg);

		try {
			if (msg instanceof HttpObject) {
				writeHttp((HttpObject) msg);
			} else {
				writeRaw((ByteBuf) msg);
			}
		} finally {
			LOG.info("Wrote: {}", msg);
		}
	}

	/**
	 * Writes HttpObjects to the connection asynchronously.
	 * 
	 * @param httpObject
	 */
	protected void writeHttp(HttpObject httpObject) {
		if (ServerUtils.isLastChunk(httpObject)) {
			channel.write(httpObject);
			LOG.info("Writing an empty buffer to signal the end of our chunked transfer");
			writeToChannel(Unpooled.EMPTY_BUFFER);
		} else {
			LOG.info("writing httpObject");
			writeToChannel(httpObject);
		}
	}

	/**
	 * Writes raw buffers to the connection.
	 * 
	 * @param buf
	 */
	protected void writeRaw(ByteBuf buf) {
		writeToChannel(buf);
	}

	protected ChannelFuture writeToChannel(final Object msg) {
		return channel.writeAndFlush(msg).addListener(ChannelFutureListener.CLOSE);
	}

	public Channel getChannel() {
		return channel;
	}

	public void writeJson(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonResponse = "";
		FullHttpResponse msg = null;
		try {
			if (object != null) {
				jsonResponse = mapper.writeValueAsString(object);
				msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, jsonResponse);
			} else {
				msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, "");
			}
			
			HttpHeaders respHeaders = null;
			respHeaders = msg.headers();
			respHeaders.set(HttpHeaderNames.CONTENT_LENGTH, msg.content().readableBytes());
			respHeaders.set(HttpHeaderNames.CONTENT_TYPE, "application/json");
			respHeaders.set(HttpHeaderNames.PRAGMA, "no-cache");
			respHeaders.set(HttpHeaderNames.CACHE_CONTROL, X_CACHE_VALUE);
			//respHeaders.set(HttpHeaderNames.ACCESS_CONTROL_ALLOW_CREDENTIALS, X_ALLOW_CONTROL_VALUE);
			//respHeaders.set(HttpHeaderNames.ACCESS_CONTROL_ALLOW_METHODS, "GET,POST");
			respHeaders.set("P3P",X_P3P_VALUE);

			LOG.info("msg {}", msg);
		} catch (JsonProcessingException e) {
			LOG.error("JsonProcessingException:", e);
			msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, "");
		}

		this.write(msg);
		this.getChannel().close();
		return;
	}

	public void writeText(String text) {
		FullHttpResponse msg = null;
		try {
			if (!StringUtils.isEmpty(text)) {
				msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, text);
			} else {
				msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, "");
			}

			HttpHeaders respHeaders = null;
			respHeaders = msg.headers();
			respHeaders.set(HttpHeaderNames.CONTENT_LENGTH, msg.content().readableBytes());
			respHeaders.set(HttpHeaderNames.CONTENT_TYPE, "text/html");
			respHeaders.set(HttpHeaderNames.PRAGMA, "no-cache");
			respHeaders.set(HttpHeaderNames.CACHE_CONTROL, X_CACHE_VALUE);
			//respHeaders.set(HttpHeaderNames.ACCESS_CONTROL_ALLOW_CREDENTIALS, X_ALLOW_CONTROL_VALUE);
			//respHeaders.set(HttpHeaderNames.ACCESS_CONTROL_ALLOW_METHODS, "GET,POST");
			respHeaders.set("P3P",X_P3P_VALUE);

			LOG.info("msg {}", msg);
		} catch (Exception e) {
			LOG.error("Exception:", e);
			msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, "");
		}

		this.write(msg);
		this.getChannel().close();
		return;
	}

	public void writeError(String text) {
		FullHttpResponse msg = null;
		try {
			msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, text);

			LOG.info("msg {}", msg);
		} catch (Exception e) {
			LOG.error("Exception:", e);
			msg = HttpResponseHandler.responseFor(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, "");
		}

		this.write(msg);
		this.getChannel().close();
		return;
	}
}
