package com.vdopia.bootstrap;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.aws.AWSConfigDataDownloader;
import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.health.HealthHandler;

import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Slf4JLoggerFactory;

public class DFilterServer {

	static {
		InternalLoggerFactory.setDefaultFactory(new Slf4JLoggerFactory());
	}

	private static final Logger logger = LoggerFactory.getLogger(DFilterServer.class.getName());
	private static final Logger criticallogger = LoggerFactory.getLogger("critical");

	private int tcpport = 9000;
	private int httpport = 9090;

	public void start() {
		try {

			logger.info("Starting DFilterServer");
			this.tcpport = Configuration.getConfig().getInt("dfilterserver.port");
			this.httpport = Configuration.getConfig().getInt("dfilterserver.httpport");
			String bucketName = Configuration.getConfig().getString("dfilterserver.awsS3bucketName");
			
			FiltersSource filtersSource = new VdopiaHttpFilter();
			
			ScheduledExecutorService scheduledHealthCheckup =  createSingleScheduledExecutor();
			scheduledHealthCheckup.scheduleAtFixedRate(new HealthHandler(), 0, 1, TimeUnit.MINUTES);
			
			/**
			 * 1) Populate ObjectStore
			
			HealthHandler healthHandler = new HealthHandler();
			HealthState healthState = healthHandler.processHealthCheckRequest();
			if(healthState != HealthState.PASS){
				logger.error("Cannot Start Object Server as HealthCheck has failed");
				return;
			}
			
			ScheduledExecutorService healthRefresher = createSingleScheduledExecutor();
			healthRefresher.scheduleWithFixedDelay(healthHandler, 1, 2, TimeUnit.MINUTES);
			
			*/
		
			List<String> regionList = Configuration.getConfig().getStringList("dfilterserver.region");
			
			ObjectStoreFactory objectStoreFactory = new ObjectStoreFactory(regionList);
			
			ScheduledExecutorService scheduledAWSDownloaderForQPS = createSingleScheduledExecutor();
			scheduledAWSDownloaderForQPS.scheduleWithFixedDelay(
					new AWSConfigDataDownloader(bucketName, objectStoreFactory, 15), 0, 15, TimeUnit.MINUTES);
			
			//HttpRequestService httpRequestService = new HttpRequestService(healthHandler);
			DefaultObjectServerBootStrap httpObjectServerBootStrap = new DefaultObjectServerBootStrap();
			httpObjectServerBootStrap.plusActivityTracker(new VdopiaRequestTracker()).withPort(this.httpport)
					.withObjectFactory(objectStoreFactory).withFiltersSource(filtersSource)
					.withServiceType(ServiceType.HTTP)
					.withTransportProtocol(TransportProtocol.TCP).start();

		} catch (Exception e) {
			criticallogger.error("Exception:", e);
		}

	}

	public static ScheduledThreadPoolExecutor createSingleScheduledExecutor() {
		final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);

		Runnable task = new Runnable() {
			@Override
			public void run() {
				executor.purge();
			}
		};

		executor.scheduleWithFixedDelay(task, 60L, 60L, TimeUnit.SECONDS);

		return executor;
	}

	public static void main(String[] args) throws Exception {
		DFilterServer rest = new DFilterServer();
		rest.start();
	}

}
