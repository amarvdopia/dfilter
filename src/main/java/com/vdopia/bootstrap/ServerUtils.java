package com.vdopia.bootstrap;

import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.LastHttpContent;

public class ServerUtils {

	  public static boolean isLastChunk(final HttpObject httpObject) {
	        return httpObject instanceof LastHttpContent;
	    }
}
