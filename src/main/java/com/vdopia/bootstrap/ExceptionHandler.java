package com.vdopia.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * This class handles any exception that occurs in netty server pipeline. This is a stateless class.
 * Hence it is shareable and is singleton
 *
 */

@Sharable
public class ExceptionHandler extends ChannelInboundHandlerAdapter {

  private final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

  private static final Logger criticallogger = LoggerFactory.getLogger("critical");
 
  public ExceptionHandler() {}
  
  /*
   * (non-Javadoc)
   * 
   * @see io.netty.channel.ChannelInboundHandlerAdapter#exceptionCaught(io.netty.channel.
   * ChannelHandlerContext, java.lang.Throwable)
   */
  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    
  }

}
