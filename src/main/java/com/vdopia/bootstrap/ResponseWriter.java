package com.vdopia.bootstrap;

import static io.netty.handler.codec.http.HttpHeaders.Names.ACCESS_CONTROL_ALLOW_CREDENTIALS;
import static io.netty.handler.codec.http.HttpHeaders.Names.ACCESS_CONTROL_ALLOW_METHODS;
import static io.netty.handler.codec.http.HttpHeaders.Names.ACCESS_CONTROL_ALLOW_ORIGIN;
import static io.netty.handler.codec.http.HttpHeaders.Names.ACCESS_CONTROL_REQUEST_HEADERS;
import static io.netty.handler.codec.http.HttpHeaders.Names.CACHE_CONTROL;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaders.Names.EXPIRES;
import static io.netty.handler.codec.http.HttpHeaders.Names.PRAGMA;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.util.DateUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.util.CharsetUtil;

/**
 * Class containing various utility functions to send various types of HTTP
 * responses
 * 
 * @author root
 * 
 */
public class ResponseWriter {

	private static final Logger logger = LoggerFactory.getLogger(ResponseWriter.class.getName());

	// Static headers and their values
	private static final CharSequence X_P3P_NAME = HttpHeaders.newEntity("P3P");
	private static final CharSequence X_P3P_VALUE = HttpHeaders.newEntity(
			"policyref=\"http://www.vdopia.com/w3c/p3p.xml\", CP=\"NOI DSP COR CURa ADMa DEVa PSAo PSDo OUR BUS UNI COM NAV STA\"");

	private static final CharSequence X_PRAGMA_NAME = HttpHeaders.newEntity(PRAGMA);
	private static final CharSequence X_PRAGMA_VALUE = HttpHeaders.newEntity("no-cache");

	private static final CharSequence X_CACHE_NAME = HttpHeaders.newEntity(CACHE_CONTROL);
	private static final CharSequence X_CACHE_VALUE = HttpHeaders
			.newEntity("no-store, no-cache, must-revalidate, post-check=0, pre-check=0");

	private static final CharSequence X_CONTENT_LENGTH_NAME = HttpHeaders.newEntity(CONTENT_LENGTH);

	private static final CharSequence X_CONTENT_TYPE_NAME = HttpHeaders.newEntity(CONTENT_TYPE);

	private static final CharSequence X_EXPIRES_NAME = HttpHeaders.newEntity(EXPIRES);

	private static final CharSequence X_ALLOW_CONTROL_NAME = HttpHeaders.newEntity(ACCESS_CONTROL_ALLOW_ORIGIN);
	private static final CharSequence X_ALLOW_CONTROL_VALUE = HttpHeaders.newEntity("*");

	private static final CharSequence X_CONTROL_ALLOW_CREDENTIALS_NAME = HttpHeaders
			.newEntity(ACCESS_CONTROL_ALLOW_CREDENTIALS);
	private static final CharSequence X_CONTROL_ALLOW_CREDENTIALS_VALUE = HttpHeaders.newEntity("true");

	private static final CharSequence X_CONTROL_ALLOW_METHODS_NAME = HttpHeaders
			.newEntity(ACCESS_CONTROL_ALLOW_METHODS);

	private static final CharSequence X_CONTROL_REQUEST_HEADERS_NAME = HttpHeaders
			.newEntity(ACCESS_CONTROL_REQUEST_HEADERS);

	private static final CharSequence X_CONNECTION_NAME = HttpHeaders.newEntity(CONNECTION);
	private static final CharSequence X_CONNECTION_VALUE_KA = HttpHeaders.newEntity(HttpHeaders.Values.KEEP_ALIVE);
	private static final CharSequence X_CONNECTION__VALUE_CLOSE = HttpHeaders.newEntity(HttpHeaders.Values.CLOSE);

	private static final CharSequence X_SI_NAME = "si";
	private static final CharSequence X_SI_VALUE = "1";
	private static final CharSequence X_TSTAMP;
	static Random random = new Random();

	static {
		Date date = new Date();
		date.setYear(date.getYear() - 1);
		String timeStamp = DateUtils.getTime(date);
		X_TSTAMP = HttpHeaders.newEntity(timeStamp);
	}

	/**
	 * Sends HTTP response status with empty response body. Also removes entries
	 * from global maps depending upon the value of removeFromMaps parameter
	 * 
	 * @param callID
	 *            callID for which global map entries need to be removed
	 * @param ctx
	 *            channel handler context
	 * @param request
	 *            request to hudson
	 * @param status
	 *            HTTP status to be sent in response
	 * @param removeFromMaps
	 *            whether entries from global maps need to be removed or not
	 */
	public static void sendResponseStatus(ChannelHandlerContext ctx, HttpRequest request,
			HttpResponseStatus status) {
		sendHttpStringResponse(ctx, request, status, null, null);
	}

	/**
	 * Sends plain text response. Also removes entries from global maps
	 * depending upon the value of removeFromMaps parameter
	 * 
	 * @param callID
	 *            callID for which global map entries need to be removed
	 * @param ctx
	 *            channel handler context
	 * @param request
	 *            request to hudson
	 * @param status
	 *            HTTP status to be sent in response
	 * @param responseBody
	 *            text in response body
	 * @param removeFromMaps
	 *            whether entries from global maps need to be removed or not
	 */
	public static void sendTextResponse(ChannelHandlerContext ctx, HttpRequest request,
			HttpResponseStatus status, String responseBody) {
		sendHttpStringResponse(ctx, request, status, responseBody, "text/plain; charset=UTF-8");
	}

	/**
	 * Sends response in format as specified in format parameter value
	 * 
	 * @param ctx
	 *            channel handler context
	 * @param request
	 *            request to hudson
	 * @param status
	 *            HTTP status to be sent in response
	 * @param responseBody
	 *            string in response body
	 * @param format
	 *            format of the response
	 */
	public static void sendResponse(ChannelHandlerContext ctx, HttpRequest request, HttpResponseStatus status,
			String responseBody) {
		sendResponse(ctx, request, status, responseBody, ResponseType.XHTML);
	}

	/**
	 * Sends response to client. Also removes entries from global maps depending
	 * upon the value of removeFromMaps parameter
	 * 
	 * @param callID
	 *            callID for which global map entries need to be removed
	 * @param ctx
	 *            channel handler context
	 * @param request
	 *            request to hudson
	 * @param status
	 *            HTTP status to be sent in response
	 * @param responseBody
	 *            string in response body
	 * @param format
	 *            format of the response
	 * @param removeFromMaps
	 *            whether entries from global maps need to be removed or not
	 * @param isSI
	 *            whether the response is a valid si(served inventory) or not
	 */
	public static void sendResponse(ChannelHandlerContext ctx, HttpRequest request,
			HttpResponseStatus status, String responseBody, ResponseType format) {

		switch (format) {
		case JS:
			sendHttpStringResponse(ctx, request, status, responseBody, "text/javascript");
			break;
		case HTML:
			sendHttpStringResponse(ctx, request, status, responseBody, "text/html");
			break;
		case JSON:
			sendHttpStringResponse(ctx, request, status, responseBody, "application/json");
			break;
		case VAST:
		case XHTML:
		case JAVASCRIPT:
		case VASTWRAPPER:
			sendHttpStringResponse(ctx, request, status, responseBody, "text/xml");
			break;
		default:
			sendTextResponse(ctx, request, OK, "ResponseType not supported");
			break;
		}
	}

	/**
	 * Sends response to client with string in response body
	 * 
	 * @param callID
	 *            callID for which global map entries need to be removed
	 * @param ctx
	 *            channel handler context
	 * @param request
	 *            request to hudson
	 * @param status
	 *            HTTP status to be sent in response
	 * @param responseBody
	 *            string to be sent in response body
	 * @param contentType
	 *            type of content
	 * @param removeFromMaps
	 *            whether entries from global maps need to be removed or not
	 * @param isSI
	 *            whether the response is a valid si(served inventory) or not
	 */
	private static void sendHttpStringResponse(ChannelHandlerContext ctx, HttpRequest request,
			HttpResponseStatus status, String responseBody, String contentType) {
		sendHttpResponse(ctx, request, status, responseBody, contentType,false);
	}

	/**
	 * Sends response to client. Also removes entries from global maps depending
	 * upon the value of removeFromMaps parameter This function create http
	 * response, sets various response headers and response body
	 * 
	 * @param callID
	 *            callID for which global map entries need to be removed
	 * @param ctx
	 *            channel handler context
	 * @param request
	 *            request to hudson
	 * @param status
	 *            HTTP status to be sent in response
	 * @param responseBody
	 *            can be string or byte array
	 * @param contentType
	 *            type of content
	 * @param removeFromMaps
	 *            whether entries from global maps need to be removed or not
	 * @param isSI
	 *            whether the response is a valid si(served inventory) or not
	 */
	private static void sendHttpResponse(ChannelHandlerContext ctx, HttpRequest request,
			HttpResponseStatus status, Object responseBody, String contentType,
			boolean isByteBuffer) {

		HttpResponse response = null;
		HttpHeaders respHeaders = null;
		if (responseBody != null) {
			ByteBuf buf = null;
			byte[] byteArr;
			if (true == isByteBuffer) {
				byteArr = (byte[]) responseBody;
				buf = Unpooled.copiedBuffer(byteArr);
			} else {
				// String resp = (String) responseBody;
				// byteArr = resp.getBytes(CharsetUtil.UTF_8);
				buf = Unpooled.copiedBuffer((String) responseBody, CharsetUtil.UTF_8);
			}
			// buf = ctx.alloc().buffer(byteArr.length).writeBytes(byteArr);
			FullHttpResponse fullHttpResponse = new DefaultFullHttpResponse(HTTP_1_1, status, buf, false);
			respHeaders = fullHttpResponse.headers();
			respHeaders.set(X_CONTENT_LENGTH_NAME, fullHttpResponse.content().readableBytes());
			respHeaders.set(X_CONTENT_TYPE_NAME, contentType);
			response = fullHttpResponse;
		} else {
			response = new DefaultHttpResponse(HTTP_1_1, status, false);
			respHeaders = response.headers();
			respHeaders.set(X_CONTENT_LENGTH_NAME, 0);
		}

		respHeaders.set(X_EXPIRES_NAME, X_TSTAMP);
		respHeaders.set(X_PRAGMA_NAME, X_PRAGMA_VALUE);
		respHeaders.set(X_CACHE_NAME, X_CACHE_VALUE);


		boolean isKeepAlive = false;
		String origin = null;
		String measure = null;
		String nginxMeasure = null;
		if (request != null) {
			HttpHeaders reqHeaders = request.headers();

			/*
			 * if (reqHeaders.contains("X-Requested-With")) { String reqWith =
			 * reqHeaders.get("X-Requested-With"); if ((null != reqWith &&
			 * !reqWith.equals(null)) &&
			 * reqWith.equalsIgnoreCase("XMLHttpRequest")) {
			 * 
			 * } }
			 */

			if (origin != null && !origin.isEmpty()) {
				respHeaders.set(X_CONTROL_ALLOW_CREDENTIALS_NAME, X_CONTROL_ALLOW_CREDENTIALS_VALUE);
				respHeaders.set(X_ALLOW_CONTROL_NAME, origin);
			} else {
				respHeaders.set(X_ALLOW_CONTROL_NAME, X_ALLOW_CONTROL_VALUE);
			}

			if (reqHeaders != null && reqHeaders.contains(X_CONTROL_REQUEST_HEADERS_NAME)) {
				String reqHead = reqHeaders.get(X_CONTROL_REQUEST_HEADERS_NAME);

				if (null != reqHead && !reqHead.equals(null)) {
					respHeaders.set(X_CONTROL_REQUEST_HEADERS_NAME, reqHead);
				} else {
					respHeaders.set(X_CONTROL_REQUEST_HEADERS_NAME, "X-Requested-With, Content-Type, Content-Length");
				}
			}

			respHeaders.set(X_CONTROL_ALLOW_METHODS_NAME, "GET,POST");

			respHeaders.set(X_P3P_NAME, X_P3P_VALUE);

			isKeepAlive = setKeepAliveHeader(request, response);
		}
		sendResponse(ctx, response, isKeepAlive);
	}

	/**
	 * Sets connection header in response depending upon the keep alive header
	 * of the request
	 * 
	 * @param request
	 *            request to hudson
	 * @param response
	 *            response to be sent
	 * @return whether keep alive was set in request or not
	 */
	private static boolean setKeepAliveHeader(HttpRequest request, HttpResponse response) {
		if (HttpHeaders.isKeepAlive(request)) {
			response.headers().set(X_CONNECTION_NAME, X_CONNECTION_VALUE_KA);
			return true;
		} else {
			response.headers().set(X_CONNECTION_NAME, X_CONNECTION__VALUE_CLOSE);
			return false;
		}
	}

	/**
	 * Writes and flushes the response on channel for the request
	 * 
	 * @param ctx
	 *            channel handler context
	 * @param response
	 *            response to be sent
	 * @param isKeepAlive
	 *            whether to close the channel after writing the response or not
	 */
	private static void sendResponse(ChannelHandlerContext ctx, HttpResponse response, boolean isKeepAlive) {
		if (isKeepAlive) {
			ctx.writeAndFlush(response);
		} else {
			ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
		}
	}
}
