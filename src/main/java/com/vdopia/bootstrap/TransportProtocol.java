package com.vdopia.bootstrap;

/**
 * Enumeration of transport protocols supported by LittleProxy.
 */
public enum TransportProtocol {
    TCP;
}