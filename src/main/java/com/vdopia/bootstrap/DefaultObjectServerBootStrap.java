package com.vdopia.bootstrap;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.health.HealthHandler;

import io.netty.channel.ChannelInboundHandlerAdapter;

public class DefaultObjectServerBootStrap implements ServerBootStrap {

	private TransportProtocol transportProtocol = TransportProtocol.TCP;
	private InetSocketAddress requestedAddress;
	private int port = 0;
	private boolean allowLocalOnly = true;
	private ObjectStoreFactory objectStoreFactory = null;
	private FiltersSource filtersSource = new FiltersSourceAdapter();
	private boolean transparent = false;
	private int idleConnectionTimeout = 120; // 70;
	private Collection<ActivityTracker> activityTrackers = new ConcurrentLinkedQueue<ActivityTracker>();
	private int connectTimeout = 10000;
	private InetSocketAddress localAddress;
	private HealthHandler healthHandler;
	private ServiceType serviceType;

	public DefaultObjectServerBootStrap(TransportProtocol transportProtocol, InetSocketAddress requestedAddress,
			int port, boolean allowLocalOnly, boolean authenticateSslClients, ObjectStoreFactory objectStoreFactory,
			HealthHandler healthHandler,
			ServiceType serviceType,
			FiltersSource filtersSource, boolean transparent, int idleConnectionTimeout,
			Collection<ActivityTracker> activityTrackers, int connectTimeout, long readThrottleBytesPerSecond,
			long writeThrottleBytesPerSecond, InetSocketAddress localAddress) {
		super();
		this.transportProtocol = transportProtocol;
		this.requestedAddress = requestedAddress;
		this.port = port;
		this.allowLocalOnly = allowLocalOnly;
		this.objectStoreFactory = objectStoreFactory;
		this.healthHandler = healthHandler;
		this.filtersSource = filtersSource;
		this.transparent = transparent;
		this.serviceType = serviceType;
		this.idleConnectionTimeout = idleConnectionTimeout;
		if (activityTrackers != null) {
			this.activityTrackers.addAll(activityTrackers);
		}
		this.connectTimeout = connectTimeout;
		this.localAddress = localAddress;
	}

	public DefaultObjectServerBootStrap() {
		// TODO Auto-generated constructor stub
	}

	public ServerBootStrap withTransportProtocol(TransportProtocol transportProtocol) {
		this.transportProtocol = transportProtocol;
		return this;
	}

	public ServerBootStrap withAddress(InetSocketAddress address) {
		this.requestedAddress = address;
		return this;
	}

	public ServerBootStrap withPort(int port) {
		this.requestedAddress = null;
		this.port = port;
		return this;
	}

	public ServerBootStrap withAllowLocalOnly(boolean allowLocalOnly) {
		this.allowLocalOnly = allowLocalOnly;
		return this;
	}
	
	public ServerBootStrap withFiltersSource(FiltersSource filtersSource) {
		this.filtersSource = filtersSource;
		return this;
	}

	public ServerBootStrap withTransparent(boolean transparent) {
		this.transparent = transparent;
		return this;
	}

	public ServerBootStrap withIdleConnectionTimeout(int idleConnectionTimeout) {
		this.idleConnectionTimeout = idleConnectionTimeout;
		return this;
	}

	public ServerBootStrap withConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
		return this;
	}

	public ServerBootStrap plusActivityTracker(ActivityTracker activityTracker) {
		activityTrackers.add(activityTracker);
		return this;
	}

	public ServerBootStrap withNetworkInterface(InetSocketAddress inetSocketAddress) {
		this.localAddress = inetSocketAddress;
		return this;
	}
	
	public ServerBootStrap withServiceType(ServiceType serviceType){
		this.serviceType = serviceType;
		return this;
	}

	public ExchangeServer start() {
		 return build().start();
	}

	private InetSocketAddress determineListenAddress() {
		return new InetSocketAddress(this.port);
	}

	public DefaultExchangeServer build() {
        return new DefaultExchangeServer(
                transportProtocol, 
                determineListenAddress(),
                objectStoreFactory,
                healthHandler,
                serviceType,
                filtersSource, transparent,
                idleConnectionTimeout, activityTrackers, connectTimeout,
                localAddress);
    }

	@Override
	public ServerBootStrap withObjectFactory(ObjectStoreFactory objectStoreFactory) {
		this.objectStoreFactory = objectStoreFactory;
		return this;
	}
	
	@Override
	public ServerBootStrap withHealthHandler(HealthHandler healthHandler) {
		this.healthHandler = healthHandler;
		return this;
	}
	
}
