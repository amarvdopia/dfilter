package com.vdopia.bootstrap;

public enum RequestType {

	DFILTER,HEALTHSTATE,VERSION,CONFIGURE,NONE;

	public static RequestType getCallType(final String url) {
		String lowerCaseUrl = url.toLowerCase();
		if (lowerCaseUrl.contains("/dfilter/getBidders"))
			return RequestType.DFILTER;
		else if (lowerCaseUrl.contains("/heathcheck"))
			return RequestType.HEALTHSTATE;
		else if (lowerCaseUrl.contains("/version"))
			return RequestType.VERSION;
		else if (lowerCaseUrl.contains("/configure"))
			return RequestType.CONFIGURE;
		else
			return RequestType.NONE;
	}

}
