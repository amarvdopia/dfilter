package com.vdopia.bootstrap;

import java.net.InetSocketAddress;

import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.health.HealthHandler;

public interface ServerBootStrap  {

    
    ServerBootStrap withTransportProtocol(
            TransportProtocol transportProtocol);

    ServerBootStrap withAddress(InetSocketAddress address);

    ServerBootStrap withPort(int port);

    ServerBootStrap withAllowLocalOnly(boolean allowLocalOnly);

    ServerBootStrap withObjectFactory(
    		ObjectStoreFactory objectStoreFactory);
    
    ServerBootStrap withHealthHandler(HealthHandler handler);

    ServerBootStrap withFiltersSource(
            FiltersSource filtersSource);
    
    ServerBootStrap withServiceType(ServiceType serviceType);

    ServerBootStrap withTransparent(
            boolean transparent);

    ServerBootStrap withIdleConnectionTimeout(
            int idleConnectionTimeout);

    ServerBootStrap withConnectTimeout(
            int connectTimeout);

    ServerBootStrap plusActivityTracker(ActivityTracker activityTracker);

    ServerBootStrap withNetworkInterface(InetSocketAddress inetSocketAddress);

    ExchangeServer start();
   
}
