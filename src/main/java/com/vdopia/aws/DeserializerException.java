package com.vdopia.aws;

public class DeserializerException extends Exception {
	
	public DeserializerException(String message) {
		super(message);
	}

}
