package com.vdopia.aws;

import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.data.ObjectStore;
import com.vdopia.protocol.KryoSerializer;

public abstract class Deserializer {
	
	private static final Logger logger = LoggerFactory.getLogger(Deserializer.class.getName());
	
	public Object readObjectFromFile(String fileName, Class objectType) throws DeserializerException {
		
		try {
			return KryoSerializer.readObjectFromFile(ObjectStore.FILE_STORE, fileName, objectType);
		} catch (FileNotFoundException e) {
			logger.error("Exception in Deserializer: {}", e);
			throw new DeserializerException(e.getMessage());
		}
	}

}
