package com.vdopia.aws;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.bootstrap.Configuration;
import com.vdopia.data.DFilterObjectStore;
import com.vdopia.data.ObjectEnum;
import com.vdopia.data.ObjectStore;
import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.util.VdopiaFileUtils;

public class AWSConfigDataDownloader implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(AWSConfigDataDownloader.class.getName());

	private final AmazonS3Reader amazonS3Reader;
	private final String serializedFilePath; 

	private final LinkedHashMap<ObjectEnum, String> tenMinsIntervalMap = new LinkedHashMap<ObjectEnum, String>();

	private final Map<Integer, LinkedHashMap<ObjectEnum, String>> schedulerIntervalMap = new ConcurrentHashMap<Integer, LinkedHashMap<ObjectEnum, String>>();
	private final String bucketName;

	private final ObjectStoreFactory objectStoreFactory;
	private final Integer intervalInMins;

	public AWSConfigDataDownloader(String bucketName, ObjectStoreFactory objectStoreFactory, Integer intervalInMins) {
		
		String accessKey = Configuration.getConfig().getString("dfilterserver.awsS3accessKey");
		String secretKey = Configuration.getConfig().getString("dfilterserver.awsS3secretKey");
	
		this.amazonS3Reader = new AmazonS3Reader(accessKey,secretKey);
		this.bucketName = bucketName;
		this.objectStoreFactory = objectStoreFactory;
		this.intervalInMins = intervalInMins;
		this.serializedFilePath = Configuration.getConfig().getString("dfilterserver.filestorepath");

		addEntriesIntoMap();
	}

	private void addEntriesIntoMap() {

		tenMinsIntervalMap.put(ObjectEnum.DFILTERSETTING,
				Configuration.getConfig().getString("dfilterserver.dfilterSettingsKey"));
		
		tenMinsIntervalMap.put(ObjectEnum.DFILTER,
				Configuration.getConfig().getString("dfilterserver.dFilterS3DumpKey"));

		schedulerIntervalMap.put(10, tenMinsIntervalMap);
	}

	public void run() {

		logger.info("Inside init AWSDownloadPoller !!!!");

		LinkedHashMap<ObjectEnum, String> map = schedulerIntervalMap.get(intervalInMins);
		for (ObjectEnum key : map.keySet()) {
			
			if(key==ObjectEnum.DFILTER){
				DFilterObjectStore objectStore = (DFilterObjectStore) objectStoreFactory.getConfigDataStore(key);
				objectStore.populateDataStore(null);
			}else{
				_run(key, map.get(key));
			}
		}

	}

	private void _run(ObjectEnum key, String fileName) {

		try {
			String zipFilePath = serializedFilePath + "/" + fileName;

			logger.info("Reading file {}",zipFilePath);
			
			File files = new File(serializedFilePath);
			if (!files.exists()) {
				if (files.mkdirs()) {
					logger.info("File store directory is created.");
				}
			}

			logger.info("Reading bucketName {} fileName {}",bucketName,fileName);
			
			amazonS3Reader.readFromS3ToFile(bucketName, fileName, zipFilePath);
			
			VdopiaFileUtils.unZipIt(zipFilePath, serializedFilePath);

			logger.info("starting populateDataStore {}",key);
			
			// Populate Data Store with Live Data
			populateDataStore(key);

		} catch (Exception e) {
			logger.error("Error: {}", e);
		}
	}

	private void populateDataStore(ObjectEnum objectValue) throws DeserializerException {

		ObjectStore objectStore = objectStoreFactory.getConfigDataStore(objectValue);
		objectStore.loadDataStore();

	}
}
