package com.vdopia.aws;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AmazonS3Util {

	private static final Logger logger = LoggerFactory.getLogger(AmazonS3Util.class);
	
	public static final int BUFFER_SIZE = 64 * 1024;

	private AmazonS3 amazons3 = null;

	public void initializeS3Client() {

		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider().getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (~/.aws/credentials), and is in valid format.", e);
		}

		amazons3 = new AmazonS3Client(credentials);
	}

	public boolean putObject(String bucketName, String key, InputStream contentsAsStream) {

		try {
			//byte[] contentAsBytes = value.getBytes("UTF-8");
			//ByteArrayInputStream contentsAsStream = new ByteArrayInputStream(contentAsBytes);
			ObjectMetadata md = new ObjectMetadata();
			//md.setContentLength(contentAsBytes.length);
			amazons3.putObject(new PutObjectRequest(bucketName, key, contentsAsStream, md));
			return true;
		} catch (AmazonServiceException e) {
			logger.error(e.getMessage(), e);
			return false;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			return false;
		}
	}
	
	public String getObject(String bucketName, String key){
		String response = null;
		try{
			S3Object object = amazons3.getObject(new GetObjectRequest(bucketName, key));
			InputStream input = object.getObjectContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
	        String line=null;
			while ((line=reader.readLine())!=null) {
	           // String line = reader.readLine();
	           // if (line == null)
	           // 	break;
	            response = response + line;
	        }
	        
		}catch(AmazonServiceException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return response;
	}
	
	public void readS3ObjectUsingByteArray(String bucketName, String key, String outputFileName) throws IOException {
	    S3Object s3object = amazons3.getObject(new GetObjectRequest(
	            bucketName, key));

	    InputStream stream = s3object.getObjectContent();
	    byte[] content = new byte[BUFFER_SIZE];

	    BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFileName));
	    int totalSize = 0;
	    int bytesRead;
	    while ((bytesRead = stream.read(content)) != -1) {
	      System.out.println(String.format("%d bytes read from stream", bytesRead));
	      outputStream.write(content, 0, bytesRead);
	      totalSize += bytesRead;
	    }
	    System.out.println("Total Size of file in bytes = "+totalSize);
	    // close resource even during exception
	    outputStream.close();
	  }

}
