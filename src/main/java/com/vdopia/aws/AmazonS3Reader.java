package com.vdopia.aws;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

/**
 * This class reads a specified object/file from S3 and writes locally
 */
public class AmazonS3Reader {

	private static final Logger logger = LoggerFactory.getLogger(AmazonS3Reader.class);

	public static final int BUFFER_SIZE = 1*1024*1024;

	private static AmazonS3 amazons3;

	private String accessKey;

	private String secretKey;
	
	public AmazonS3Reader(String accessKey,String secretKey) {
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		init(accessKey, secretKey);
	}

	public void init(String accessKey, String secretKey) {
		ClientConfiguration clientConfiguration = new ClientConfiguration()
		        .withMaxErrorRetry(5) // 10 retries
		        .withConnectionTimeout(50000) // 50,000 ms
		        .withSocketTimeout(50000); // 50,000 ms
		
		amazons3 = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey),clientConfiguration);
	}

	/**
	 * Read a character oriented file from S3 to fullFilePath
	 *
	 * @param bucketName
	 * @param key
	 * @param fullFilePath
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void readFromS3ToFile(String bucketName, String key, String fullFilePath) throws IOException, InterruptedException {
		S3Object s3object = amazons3.getObject(new GetObjectRequest(bucketName, key));
		logger.info(s3object.getObjectMetadata().getContentType());
		logger.info("{}",s3object.getObjectMetadata().getContentLength());

		InputStream in = null;
		OutputStream out = null;
		try {
			in = s3object.getObjectContent();
			byte[] buf = new byte[BUFFER_SIZE];
			out = new FileOutputStream(fullFilePath);
			int count;
			while ((count = in.read(buf)) != -1) {
				if (Thread.interrupted()) {
					throw new InterruptedException();
				}
				out.write(buf, 0, count);
			}

		} catch (Exception e) {
			logger.error("Exception ", e);
			throw new IOException(e);
		} finally {
			if (null != in) {
				try{in.close();}catch(Exception e){}
			}
			if (null != out) {
				try{out.close();}catch(Exception e){}
			}
		}

	}


	/**
	 * Read a binary file from S3
	 *
	 * @param bucketName
	 *            bucket name
	 * @param key
	 *            file name
	 * @throws IOException
	 */
	public int readS3ObjectSizeByteArray(String bucketName, String key) throws IOException {
		S3Object s3object = amazons3.getObject(new GetObjectRequest(bucketName, key));

		InputStream stream = s3object.getObjectContent();
		byte[] content = new byte[BUFFER_SIZE];

		int totalSize = 0;

		int bytesRead;
		while ((bytesRead = stream.read(content)) != -1) {
			logger.info(String.format("%d bytes read from stream", bytesRead));
			totalSize += bytesRead;
		}
		logger.info("Total Size of file in bytes = " + totalSize);

		return totalSize;
	}

	/**
	 * Read a binary file from S3
	 *
	 * @param bucketName
	 *            bucket name
	 * @param key
	 *            file name
	 * @throws IOException
	 */
	public void readS3ObjectUsingByteArray(String bucketName, String key, String outputFileName) throws IOException {
		S3Object s3object = amazons3.getObject(new GetObjectRequest(bucketName, key));

		InputStream stream = s3object.getObjectContent();
		byte[] content = new byte[BUFFER_SIZE];

		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFileName));

		int totalSize = 0;

		int bytesRead;
		while ((bytesRead = stream.read(content)) != -1) {
			logger.info(String.format("%d bytes read from stream", bytesRead));
			outputStream.write(content, 0, bytesRead);
			totalSize += bytesRead;
		}
		logger.info("Total Size of file in bytes = " + totalSize);

		// close resource even during exception

		outputStream.close();
	}

}