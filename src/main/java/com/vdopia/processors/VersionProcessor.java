package com.vdopia.processors;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.bootstrap.WriteHandler;
import com.vdopia.util.VersionNumber;

public class VersionProcessor implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(VersionProcessor.class.getName());

	@Override
	public void process(Map<String, String> queryParameters, WriteHandler writeHandler) {
		String versionNumber = VersionNumber.getProperty("version_number").trim();
		logger.trace("Request terminated because of callType : version");
		writeHandler.writeText(versionNumber);
		return;
	}

}
