package com.vdopia.processors;

import java.util.List;
import java.util.Map;

import com.vdopia.bootstrap.WriteHandler;

public interface Processor {

	public void process(Map<String, String> queryParameters,WriteHandler writeHandler);
		
}
