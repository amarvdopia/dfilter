package com.vdopia.processors;

import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vdopia.bootstrap.WriteHandler;
import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.dfilter.DFilterHandler;
import com.vdopia.dfilter.DFilterInputData;

public class DFilterProcessor{
	
	private ObjectStoreFactory objectStoreFactory;

	public DFilterProcessor(ObjectStoreFactory objectStoreFactory){
		this.objectStoreFactory = objectStoreFactory;
	}

	public void process(String payLoad,Map<String, String> queryParameters,WriteHandler writeHandler) {
		
		String bidderJson =payLoad;
		Gson gson = new Gson();
		DFilterInputData dfilterInput = gson.fromJson(bidderJson, new TypeToken<DFilterInputData>(){}.getType());
		
		DFilterHandler dhandler = new DFilterHandler(objectStoreFactory);
		List<String> selectedBidders = dhandler.getBidders(dfilterInput);
		
		writeHandler.writeJson(selectedBidders);

		return;
		
	}

}
