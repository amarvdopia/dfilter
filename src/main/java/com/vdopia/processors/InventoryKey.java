package com.vdopia.processors;

public class InventoryKey {

	private String bidderId;
	private String apiKey;
	private String propertyName;
	private String country;
	private String metroCode;
	
	private String day;
	private String hour;
	private String quarter;
	
	
	/**
	 * @return the bidderId
	 */
	public String getBidderId() {
		return bidderId;
	}
	/**
	 * @param bidderId the bidderId to set
	 */
	public void setBidderId(String bidderId) {
		this.bidderId = bidderId;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	/**
	 * @return the metroCode
	 */
	public String getMetroCode() {
		return metroCode;
	}
	/**
	 * @param metroCode the metroCode to set
	 */
	public void setMetroCode(String metroCode) {
		this.metroCode = metroCode;
	}
	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}
	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}
	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}
	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}
	/**
	 * @return the quarter
	 */
	public String getQuarter() {
		return quarter;
	}
	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	
}
