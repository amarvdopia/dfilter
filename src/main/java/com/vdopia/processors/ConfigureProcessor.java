package com.vdopia.processors;

import java.util.Map;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.bootstrap.WriteHandler;
import com.vdopia.health.HealthCheck;

public class ConfigureProcessor implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(ConfigureProcessor.class.getName());

	@Override
	public void process(Map<String, String> queryParameters, WriteHandler writeHandler) {

		String status = this.configure(queryParameters);
		logger.trace("Request terminated because of callType : CONFIGURE , status : {}", status);
		writeHandler.writeText(status);
		return;
	}
	
	private synchronized String configure(Map<String, String> queryParameters) {
		StringBuilder builder = new StringBuilder("Following are confgured: \n");

		if (queryParameters.containsKey("loggerlevel")) {
			String level = queryParameters.get("loggerlevel");
			level = level.toLowerCase();
			Level loggerLevel = this.getLevel(level);
			if (loggerLevel != null) {
				org.apache.log4j.Logger hudsonLogger = org.apache.log4j.Logger.getLogger("com.vdopia");
				hudsonLogger.setLevel(loggerLevel);
				builder.append("hudsonloggerlevel has been set\n");
			}
		}
		if (queryParameters.containsKey("nettyloggerlevel")) {
			String level = queryParameters.get("nettyloggerlevel");
			level = level.toLowerCase();
			Level loggerLevel = this.getLevel(level);
			if (loggerLevel != null) {
				org.apache.log4j.Logger nettyLogger = org.apache.log4j.Logger.getLogger("io.netty");
				nettyLogger.setLevel(loggerLevel);
				builder.append("nettyloggerlevel has been set\n");
			}
		}
		if (queryParameters.containsKey("rootloggerlevel")) {
			String level = queryParameters.get("rootloggerlevel");
			level = level.toLowerCase();
			Level loggerLevel = this.getLevel(level);
			if (loggerLevel != null) {
				org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
				rootLogger.setLevel(loggerLevel);
				builder.append("rootloggerlevel has been set\n");
			}
		}

		if (queryParameters.containsKey("traffic")) {
			String value = queryParameters.get("traffic");
			if (value.equalsIgnoreCase("on")) {
				HealthCheck.setTrafficOn(true);
				builder.append("Traffic is on\n");
			} else if (value.equalsIgnoreCase("off")) {
				HealthCheck.setTrafficOn(false);
				builder.append("Traffic is off\n");
			}
		}

		return builder.toString();
	}

	private Level getLevel(String level) {
		if (null == level)
			return null;
		level = level.toLowerCase();

		switch (level) {
		case "all":
			return Level.ALL;
		case "debug":
			return Level.DEBUG;
		case "error":
			return Level.ERROR;
		case "fatal":
			return Level.FATAL;
		case "info":
			return Level.INFO;
		case "off":
			return Level.OFF;
		case "trace":
			return Level.TRACE;
		case "warn":
			return Level.WARN;
		default:
			return null;
		}
	}
}
