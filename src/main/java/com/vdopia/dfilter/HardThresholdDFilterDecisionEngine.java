package com.vdopia.dfilter;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DecisionEngine for Dfilter that uses hard threshold for efficiency and
 * trickles bid-requests, i.e., it more or less follows a discontinuous graph
 * for bid-request (10%-100%)
 * 
 * @author amar
 *
 */

public class HardThresholdDFilterDecisionEngine implements DFilterDecisionEngine {
	
	Logger logger = LoggerFactory.getLogger(HardThresholdDFilterDecisionEngine.class);

	/*@Autowired
	DFilterSettingsCache dFilterSettingsCache;*/

	private float efficiencyLowerBound = DFilterSettingConstants.DEFAULT_EFFICICENCY_LOWER_BOUND;
	private int tricklePercentage = DFilterSettingConstants.DEFAULT_TRICKLE_PERCENTAGE;
	private float efficiency;

	public HardThresholdDFilterDecisionEngine(float efficiencyLowerBound, int tricklePercentage, float efficiency) {
		super();
		this.efficiencyLowerBound = efficiencyLowerBound;
		this.tricklePercentage = tricklePercentage;
		this.efficiency = efficiency;
	}

	/*public HardThresholdDFilterDecisionEngine(float efficiency) {
		super();
		
		DFilterSettings efficiencyLowerBoundSetting = dFilterSettingsCache
				.getDFilterSettings(DFilterSettingConstants.FIXED_EFFICIENCY_LOWER_BOUND);
		DFilterSettings tricklePercentageSetting = dFilterSettingsCache
				.getDFilterSettings(DFilterSettingConstants.FIXED_TRICKLE_PERCENTAGE);
		if (efficiencyLowerBoundSetting != null && efficiencyLowerBoundSetting.getSettingValue() != null) {
			this.efficiencyLowerBound = Float.parseFloat(efficiencyLowerBoundSetting.getSettingValue());
			logger.info("Using efficiency lower bound for dfilter: " + this.efficiencyLowerBound);
		}
		if (tricklePercentageSetting != null && tricklePercentageSetting.getSettingValue() != null) {
			this.tricklePercentage = Integer.parseInt(tricklePercentageSetting.getSettingValue());
			logger.info("Using trickle percentage for dfilter : " + this.tricklePercentage);
		}
		
		this.efficiency = efficiency;
	}*/

	@Override
	public boolean shouldRejectBidder() {
		boolean shouldReject = false;
		if (this.efficiency < this.efficiencyLowerBound) {
			shouldReject = isRejectedWithProbability(this.tricklePercentage);
		}

		return shouldReject;
	}

	private boolean isRejectedWithProbability(int tricklePercentage) {
		// TODO : Check random seed
		int randomNumber = new Random().nextInt(100);
		if (randomNumber <= tricklePercentage) {
			return false;
		}
		return true;

	}
	/*public static void main(String[] args) {
		HardThresholdDFilterDecisionEngine engine = new HardThresholdDFilterDecisionEngine(0.1f);
		boolean rejection = engine.shouldRejectBidder();
		System.out.println(rejection);
	}*/
	
	
}
