package com.vdopia.dfilter;

import java.util.List;

public class DFilterInputData {

	private List<String> bidders;
	private String apikey;
	private String country;
	private String locationCode;
	private String propertyName;
	
	public DFilterInputData(){
		
	}
	
	
	/**
	 * @return the bidders
	 */
	public List<String> getBidders() {
		return bidders;
	}
	/**
	 * @param bidders the bidders to set
	 */
	public void setBidders(List<String> bidders) {
		this.bidders = bidders;
	}
	/**
	 * @return the apikey
	 */
	public String getApikey() {
		return apikey;
	}
	/**
	 * @param apikey the apikey to set
	 */
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the locationCode
	 */
	public String getLocationCode() {
		return locationCode;
	}
	/**
	 * @param locationCode the locationCode to set
	 */
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	/**
	 * @return the propertyName
	 */
	public String getPropertyName() {
		return propertyName;
	}
	/**
	 * @param propertyName the propertyName to set
	 */
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	
	
}
