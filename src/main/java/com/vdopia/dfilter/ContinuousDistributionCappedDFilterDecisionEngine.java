package com.vdopia.dfilter;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DecisionEngine for Dfilter that uses sample-rate, bid-rate and efficiency to
 * calculate number of bid-request, i.e., it more or less follows a continous
 * graph for bid-request
 * 
 * @author amar
 *
 */

public class ContinuousDistributionCappedDFilterDecisionEngine implements DFilterDecisionEngine {

	Logger logger = LoggerFactory.getLogger(ContinuousDistributionCappedDFilterDecisionEngine.class);

	/*@Autowired
	DFilterSettingsCache dFilterSettingsCache;*/

	private int bidFactor = DFilterSettingConstants.DEFAULT_BID_FACTOR;
	private int sampleBidRate = DFilterSettingConstants.DEFAULT_SAMPLE_BID_RATE;

	/*private DFilterSettings sampleBidRateSetting;
	private DFilterSettings bidFactorSetting;*/
	private float efficiency;

	public ContinuousDistributionCappedDFilterDecisionEngine(int bidFactor, int sampleBidRate, float efficiency) {
		this.bidFactor = bidFactor;
		this.sampleBidRate = sampleBidRate;
		this.efficiency = efficiency;
	}

	/*public ContinuousDistributionCappedDFilterDecisionEngine(float efficiency) {
		this.sampleBidRateSetting = dFilterSettingsCache.getDFilterSettings(DFilterSettingConstants.SAMPLE_BID_RATE);
		this.bidFactorSetting = dFilterSettingsCache.getDFilterSettings(DFilterSettingConstants.BID_FACTOR);
		this.efficiency = efficiency;
	}*/

	@Override
	public boolean shouldRejectBidder() {
		boolean shouldReject = false;
		/*if (this.sampleBidRateSetting != null && this.sampleBidRateSetting.getSettingValue() != null) {
			this.bidFactor = Integer.parseInt(this.bidFactorSetting.getSettingValue());
			logger.info("Using bidfactor for dfilter: " + this.bidFactor);
		}
		if (this.bidFactorSetting != null && this.bidFactorSetting.getSettingValue() != null) {
			this.sampleBidRate = Integer.parseInt(this.sampleBidRateSetting.getSettingValue());
			logger.info("Using sampleBidRate for dfilter: " + this.sampleBidRate);
		}*/

		int monetizationProbability = Math.min(Math.max((int) (this.efficiency * bidFactor), sampleBidRate), 100);
		
		//If we are going to use below mentioned algorithm than we need to make the parameters like power and multiplication factor configurable
		//int monetizationProbability = Math.min(Math.max((int)(Math.pow((this.efficiency * 20), 2)), sampleBidRate), 100);

		shouldReject = isRejectedWithProbability(monetizationProbability);

		return shouldReject;
	}

	private boolean isRejectedWithProbability(float monetizationProbability) {
		// TODO : Check random seed
		int randomNumber = new Random().nextInt(100);
		if (randomNumber <= monetizationProbability) {
			return false;
		}
		return true;

	}

}
