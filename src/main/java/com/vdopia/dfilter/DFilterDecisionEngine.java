package com.vdopia.dfilter;

/**
 * Interface for decisionEngine that will decide whether a bid-request is 
 * to be sent to a bidder based on past performance for this inventory
 * 
 * @author amar
 *
 */
public interface DFilterDecisionEngine {
	
	public boolean shouldRejectBidder();
	
}
