package com.vdopia.dfilter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.vdopia.cache.DFilterCacheBlock;

/**
 * Class for getting filename from s3 based on the timestamp at which the file
 * is requested
 * 
 * @author amar
 *
 */

public class S3DFilterCacheBlockMapper {

	public static final String dFilterS3DumpBucket = "dFilterS3DumpBucket";
	static Properties prop = new Properties();

	public static final String getS3DFilterFileName(DFilterCacheBlock cacheBlock) {
		String dumpFileName = "dfilter.bidder_predicted_performance_" + cacheBlock.toString();
		return dumpFileName;
	}

	public static final String getS3BucketForDFilter() throws IOException {
		if (prop.get(dFilterS3DumpBucket) == null) {
			InputStream inputStream = S3DFilterCacheBlockMapper.class.getClassLoader()
					.getResourceAsStream("dfilter.properties");
			/*
			 * InputStream inputStream = new FileInputStream(
			 * "/Users/nipunsarin/codebase/hudson/cache/src/main/resources/dfilter.properties"
			 * );
			 */
			prop.load(inputStream);
		}
		return prop.getProperty(dFilterS3DumpBucket);
	}
}
