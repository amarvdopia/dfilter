package com.vdopia.dfilter;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.data.DFilterObjectStore;
import com.vdopia.data.DFilterSettingsObjectStore;
import com.vdopia.data.InventoryKeyTransformer;
import com.vdopia.data.ObjectEnum;
import com.vdopia.data.ObjectStoreFactory;
import com.vdopia.data.model.DFilterSettings;
import com.vdopia.dfilter.model.DFilterKey;
import com.vdopia.dfilter.model.DFilterPerformanceValue;
import com.vdopia.processors.InventoryKey;

public class DFilterHandler {

	private static final Logger logger = LoggerFactory.getLogger(DFilterHandler.class.getName());
	
	private ObjectStoreFactory objectStoreFactory;

	public DFilterHandler(ObjectStoreFactory oStoreFactory) {
		this.objectStoreFactory = oStoreFactory;
	}

	public List<String> getBidders(DFilterInputData dfilterInput) {

		List<String> bidderList = new ArrayList<String>();
		InventoryKeyTransformer itransformer = new InventoryKeyTransformer();
		List<InventoryKey> ikeyList = itransformer.tranform(dfilterInput);

		DFilterObjectStore<DFilterKey> objectStore = (DFilterObjectStore<DFilterKey>) objectStoreFactory
				.getConfigDataStore(ObjectEnum.DFILTER);

		/**
		 * Map the Demand 
		 */
		List<DFilterPerformanceValue> demandList = objectStore.filterDemand(ikeyList);

		logger.info("demandList:size {}",demandList.size());
				
		DFilterSettingsObjectStore<DFilterSettings> dfilterSettingsStore = (DFilterSettingsObjectStore<DFilterSettings>) objectStoreFactory
				.getConfigDataStore(ObjectEnum.DFILTERSETTING);

		/**
		 * Reduce the demand by applying Distribution Logic
		 */
		
		for (DFilterPerformanceValue dpvalue : demandList) {

			logger.info("auctions {}",dpvalue.getAuctions());
			
			DFilterSettings sbidrate = dfilterSettingsStore.getDFilterSettings(DFilterSettingConstants.SAMPLE_BID_RATE);
			
			logger.info("getSettingValue {}",sbidrate.getSettingValue());
			int sampleBidRateSetting = Integer.parseInt(sbidrate.getSettingValue());
			
			DFilterSettings sbidfactor = dfilterSettingsStore.getDFilterSettings(DFilterSettingConstants.BID_FACTOR);
			
			int bidfactor = Integer.parseInt(sbidfactor.getSettingValue());
			int ai = dpvalue.getAuctions();
			int vi = dpvalue.getImpressions();
			float efficiency = vi * 100f / ai;

			ContinuousDistributionCappedDFilterDecisionEngine cdecisionEngine = new ContinuousDistributionCappedDFilterDecisionEngine(
					bidfactor, sampleBidRateSetting, efficiency);
			
			logger.info("efficiency {} cdecisionEngine.shouldRejectBidder() {}",efficiency,cdecisionEngine.shouldRejectBidder());
			
			//if (!cdecisionEngine.shouldRejectBidder()) {
				bidderList.add(dpvalue.getBidderId());
			//}

		}

		return bidderList;

	}

}