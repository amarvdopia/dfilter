package com.vdopia.dfilter;

public class DFilterSettingConstants {
	
	public static final String FIXED_EFFICIENCY_LOWER_BOUND = "fixed_efficiency_lower_bound";
	public static final String FIXED_TRICKLE_PERCENTAGE = "fixed_trickle_percentage";
	public static final String SAMPLE_BID_RATE = "sample_bid_rate";
	public static final String BID_FACTOR = "bid_factor";
	public static final String IS_DFILTER_ENABLED = "is_enabled";
	
	public static final int DEFAULT_BID_FACTOR = 250;
	public static final int DEFAULT_SAMPLE_BID_RATE = 10;
	public static final float DEFAULT_EFFICICENCY_LOWER_BOUND = 0.1f;
	public static final int DEFAULT_TRICKLE_PERCENTAGE = 10;

}
