package com.vdopia.dfilter.model;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DFilterKey implements DFilterEntry{

	private static final Logger logger = LoggerFactory.getLogger(DFilterKey.class.getName());

	private final List<String> keyList;
	
	private final String dfilterKey;
	
	public DFilterKey(List<String> keyList) {
		super();
		this.keyList = keyList;
		this.dfilterKey = generateKey(keyList);
	}

	public String getDfilterKey() {
		return dfilterKey;
	}
	
	
	public List<String> getKeyList() {
		return keyList;
	}

	/**
	 * Formatting of the key values should be handled while adding 
	 * element in List
	 * @param keyList
	 * @return
	 */
	public String generateKey(List<String> keyList){
		StringBuffer strbuff = new StringBuffer();
		for(String key : keyList){
			strbuff.append(key+"_");
		}
		
		//logger.info("strbuff.key: {}",strbuff.toString());
		return strbuff.toString();
	}
	
}
