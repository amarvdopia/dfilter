package com.vdopia.dfilter.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class DFilterBucket<DFilterKey, DFilterValue>{

	private String label="_default";
	
	private final Map<DFilterKey,DFilterValue> provider;
	
	public DFilterBucket(){
		this.provider = new ConcurrentHashMap<>();
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Map<DFilterKey, DFilterValue> getProvider() {
		return provider;
	}

}
