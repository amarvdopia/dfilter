package com.vdopia.dfilter.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.vdopia.processors.InventoryKey;

public class DFilterBucketFactory {

	private static Map<BucketType, DFilterBucket> bucketProviderMap;

	public DFilterBucketFactory() {
		bucketProviderMap = new ConcurrentHashMap<BucketType, DFilterBucket>();
	}

	public static DFilterBucket getBucket(BucketType bucketType) {

		DFilterBucket cache = null;
		switch (bucketType) {
		case FIXED:
			cache = bucketProviderMap.get(BucketType.FIXED);
			break;
		case VAIRABLE:
			cache = bucketProviderMap.get(BucketType.VAIRABLE);
			break;
		default:
			break;
		}

		return cache;
	}

	public void registerBucket(BucketType type, DFilterBucket dFilterBucket) {
		if (this.getBucket(type) == null) {
			bucketProviderMap.put(type, dFilterBucket);
		}
	}
	
}
