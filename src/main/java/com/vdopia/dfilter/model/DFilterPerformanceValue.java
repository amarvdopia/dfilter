package com.vdopia.dfilter.model;

public class DFilterPerformanceValue implements Comparable{
	
	private final String performanceKey;
	private final String bidderId;
	private int auctions;
	private int impressions;
	private float demandRevenue;
	private float margin;
	
	public DFilterPerformanceValue(String performanceKey,String bidderId) {
		super();
		this.performanceKey = performanceKey;
		this.bidderId = bidderId;
	}
	
	public int getAuctions() {
		return auctions;
	}
	public void setAuctions(int auctions) {
		this.auctions = auctions;
	}
	public int getImpressions() {
		return impressions;
	}
	public void setImpressions(int impressions) {
		this.impressions = impressions;
	}
	
	/**
	 * @return the demandRevenue
	 */
	public float getDemandRevenue() {
		return demandRevenue;
	}

	/**
	 * @param demandRevenue the demandRevenue to set
	 */
	public void setDemandRevenue(float demandRevenue) {
		this.demandRevenue = demandRevenue;
	}

	public float getMargin() {
		return margin;
	}
	public void setMargin(float margin) {
		this.margin = margin;
	}

	public String getPerformanceKey() {
		return performanceKey;
	}
	
	/**
	 * @return the bidderId
	 */
	public String getBidderId() {
		return bidderId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + auctions;
		result = prime * result + Float.floatToIntBits(demandRevenue);
		result = prime * result + impressions;
		result = prime * result + Float.floatToIntBits(margin);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DFilterPerformanceValue other = (DFilterPerformanceValue) obj;
		if (auctions != other.auctions)
			return false;
		if (Float.floatToIntBits(demandRevenue) != Float.floatToIntBits(other.demandRevenue))
			return false;
		if (impressions != other.impressions)
			return false;
		if (Float.floatToIntBits(margin) != Float.floatToIntBits(other.margin))
			return false;
		return true;
	}

	public String toString(){
		StringBuffer strbuf = new StringBuffer();
		strbuf.append("auctions:"+this.getAuctions()+":impressions:"+this.getImpressions());
		strbuf.append("margin:"+this.getMargin()+":demandRevenue:"+this.getDemandRevenue());
		return strbuf.toString();
	}

	@Override
	public int compareTo(Object dval) {
		DFilterPerformanceValue dvalue = (DFilterPerformanceValue)dval;
		return this.getAuctions() - dvalue.getAuctions();
	}
	
}
