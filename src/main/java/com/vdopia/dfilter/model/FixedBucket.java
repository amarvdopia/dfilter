package com.vdopia.dfilter.model;

public class FixedBucket<K extends DFilterKey,V extends DFilterValue> extends DFilterBucket<DFilterKey,DFilterValue>{
	
	public FixedBucket(){
		super();
		this.setLabel("_bidder");
	}

}
