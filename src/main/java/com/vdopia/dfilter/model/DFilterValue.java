package com.vdopia.dfilter.model;

public class DFilterValue implements DFilterEntry{

	private final Object value;
	private final BucketType nextKeyState;
	
	public DFilterValue(Object dvalue,BucketType nextKeyState) {
		super();
		this.value = dvalue;
		this.nextKeyState = nextKeyState;
	}

	public Object getValue() {
		return value;
	}

	public BucketType getNextKeyState() {
		return nextKeyState;
	}
	
}
