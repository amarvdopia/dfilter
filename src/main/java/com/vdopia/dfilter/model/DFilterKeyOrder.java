package com.vdopia.dfilter.model;

public enum DFilterKeyOrder {

	BIDDERID,PROPERTYNAME,APIKEY,COUNTRY,GEOCODE,DAY,HOUR,QUARTER;

}
