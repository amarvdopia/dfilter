package com.vdopia.dfilter.model;

public class VariableBucketValue extends DFilterValue{
	
	public VariableBucketValue(DFilterPerformanceValue bucketValue){
		super(bucketValue,BucketType.LAST);
	}

}
