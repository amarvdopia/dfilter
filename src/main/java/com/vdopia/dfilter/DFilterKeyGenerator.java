package com.vdopia.dfilter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.amazonaws.util.StringUtils;
import com.vdopia.processors.InventoryKey;

//import com.vdopia.rtb.request.IRTBRequest;

/**
 * Class for generating keys to be used in the sub-maps in DfilterCache
 * 
 * @author amar
 *
 */

public class DFilterKeyGenerator {
	static {

	}

	private static DFilterKeyGenerator instance = new DFilterKeyGenerator();

	public static DFilterKeyGenerator getInstance() {
		return instance;
	}

	/*public String getKeyForMapIngestion(String bidderId, String propertyName, String apiKey, String country,
			String zipCode) {
		String key = bidderId + "-" + propertyName + "-" + apiKey + "-" + country;
		if (country.equals("USA")) {
			if (zipCode != null && !(zipCode.isEmpty())) {
				key = key + "-" + zipCode;
			}
		}
		return key;
	}
*/
	public List<String> getKeyForMapIngestion(String bidderId, String propertyName, String apiKey, String country, 
			String zipCode) {
		List<String> keyList = new ArrayList<String>(); 
		String key = bidderId + "-" + propertyName + "-" + apiKey + "-" + country; 
		keyList.add(key); 
		if (country.equals("USA")) { 
			if (!(zipCode.isEmpty())) { 
				key = key + "-" + zipCode; 
				keyList.add(0, key);
			} 
		} 
		return keyList; 
	}
	
	/*
	 * public List<String> getKeyForMapIngestion(String bidderId, String
	 * propertyName, String apiKey, String country, String zipCode) {
	 * List<String> keyList = new ArrayList<String>(); String key = bidderId +
	 * "-" + propertyName + "-" + apiKey + "-" + country; keyList.add(key); if
	 * (country.equals("USA")) { if (!(zipCode.isEmpty())) { key = key + "-" +
	 * zipCode; } } keyList.add(key); return keyList; }
	 */
	
	public String getPropertyName(InventoryKey inventory) {
		String propertyName = null;
		if (!(StringUtils.isNullOrEmpty(inventory.getPropertyName()))) {
			propertyName = inventory.getPropertyName();
		}
		return propertyName;
	}

	public String getApiKey(InventoryKey inventory) {
		String apiKey = null;
		if (!(StringUtils.isNullOrEmpty(inventory.getApiKey()))) {
			apiKey = inventory.getApiKey();
		}
		return apiKey;
	}

	public String getCountry(InventoryKey inventory) {
		String country = null;

		if (!(StringUtils.isNullOrEmpty(inventory.getCountry()))) {
			country = inventory.getCountry();
		}
		return country;
	}
	
	

	public String getZipCode(InventoryKey inventory) {
		if ( inventory.getMetroCode() != null) {
			return inventory.getMetroCode();
		} else {
			return "";
		}
	}

	/*
	 * public String generateKeyForBidder(IRTBRequest rtbRequest) { String
	 * propertyName = getPropertyName(rtbRequest); String apiKey =
	 * getApiKey(rtbRequest); String country = getCountry(rtbRequest); String
	 * zipCode = getZipCode(rtbRequest);
	 * 
	 * if (propertyName == null || apiKey == null || country == null) { return
	 * null; }
	 * 
	 * String keyForBidder = propertyName + "-" + apiKey + "-" + country; if
	 * (country.equals("USA")) { keyForBidder = keyForBidder + "-" + zipCode; }
	 * return keyForBidder;
	 * 
	 * }
	 */

	public List<String> generateKeyForBidder(InventoryKey inventory) {

		List<String> keyList = new ArrayList<String>();

		String propertyName = getPropertyName(inventory);
		String apiKey = getApiKey(inventory);
		String country = getCountry(inventory);
		String zipCode = getZipCode(inventory);

		if (propertyName == null || apiKey == null || country == null) {
			return null;
		}

		String keyForBidder = propertyName + "-" + apiKey + "-" + country;
		keyList.add(keyForBidder);
		if (country.equals("USA")) {
			if (zipCode != null && !(zipCode.isEmpty())) {
				keyForBidder = keyForBidder + "-" + zipCode;
			}
		}
		keyList.add(0, keyForBidder);
		return keyList;
	}
	
	
	public static String getCurrentQuarterTimeKey(String bprefix){
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int quarter = (cal.get(Calendar.MINUTE) / 15) + 1;
		
		
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append(bprefix).append("_")
		.append(year).append("_")
		.append(String.format("%02d",month+"")).append("_")
		.append(String.format("%02d",date+"")).append("_")
		.append(String.format("%02d",hour+"")).append("_")
		.append(quarter);
		
		return sbuilder.toString();
	}

	public static String getPreviousQuarterTimeKey(String bprefix){
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.add(Calendar.MINUTE, -15);

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int quarter = (cal.get(Calendar.MINUTE) / 15) + 1;
		
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append(bprefix).append("_")
		.append(year).append("_")
		.append(String.format("%02d",month+"")).append("_")
		.append(String.format("%02d",date+"")).append("_")
		.append(String.format("%02d",hour+"")).append("_")
		.append(quarter);
		
		return sbuilder.toString();
	}

}
