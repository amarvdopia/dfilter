package com.vdopia.dataloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.aws.AmazonS3Reader;
import com.vdopia.bootstrap.Configuration;
import com.vdopia.cache.DFilterCache;
import com.vdopia.cache.DFilterCacheBlock;
import com.vdopia.cache.DFilterCacheBlockSegregatorByQuarter;
import com.vdopia.dfilter.DFilterKeyGenerator;
import com.vdopia.dfilter.model.DFilterPerformanceValue;
import com.vdopia.util.Constants;

/**
 * Class to get bidder-perfomance data from s3 and populating it in dfiltercache. 
 * This class also handles the responsibility for deleting old caches
 * 
 * @author amar
 *
 */

public class S3dFilterFetcher implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(S3dFilterFetcher.class.getName());
	
	public static final int CACHE_CLEARING_TIME_THRESHOLD_IN_MILLIS = 7200000;
	
	public S3dFilterFetcher(){
		
	}
	
	private void loadDFilterDataInCache(File newFile, DFilterCacheBlockSegregatorByQuarter cacheBlock)
			throws IOException {
		
		logger.info("File length of fetched dfilter data dump = " + newFile.length());
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(newFile)));
		Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(reader);
		int recordNumber = 0;

		Map<String, DFilterPerformanceValue> dFilterMap = DFilterCache.getInstance().getDFilterCache(cacheBlock);
		logger.info("Ingesting S3 data in DFilterMap for Key : " + cacheBlock);
		DFilterKeyGenerator keyGenerator = DFilterKeyGenerator.getInstance();
		for (CSVRecord record : records) {
			if (recordNumber > 6) {
				try {
					// TODO Decouple this part
					String bidderId = record.get(3);
					String propertyName = record.get(4);
					String apiKey = record.get(5);
					String country = record.get(6);
					String zipCode = record.get(7);
					int ai = Integer.parseInt(record.get(8));
					int vi = Integer.parseInt(record.get(9).equals("")?"0":record.get(9));
					float revenue = Float.parseFloat(record.get(10).equals("")?"0":record.get(10));
				    float margin = Float.parseFloat(record.get(11).equals("")?"0":record.get(11));
					
					DFilterPerformanceValue bidderPerformance = new DFilterPerformanceValue(bidderId,bidderId);
					bidderPerformance.setAuctions(ai);
					bidderPerformance.setImpressions(vi);
					bidderPerformance.setDemandRevenue(revenue);
					bidderPerformance.setMargin(margin);
					//float efficiency = vi*100f/ai;
					
					//String key = keyGenerator.getKeyForMapIngestion(bidderId, propertyName, apiKey, country, zipCode);
					
					List<String> keyList = keyGenerator.getKeyForMapIngestion(bidderId, propertyName, apiKey, country, zipCode);
					for (String key : keyList) {
						DFilterPerformanceValue existingBidderPerformance = dFilterMap.get(key);
						if( existingBidderPerformance != null ) {
							existingBidderPerformance.setAuctions(existingBidderPerformance.getAuctions() + bidderPerformance.getAuctions());
							existingBidderPerformance.setImpressions(existingBidderPerformance.getImpressions() + bidderPerformance.getImpressions());
							existingBidderPerformance.setDemandRevenue(existingBidderPerformance.getDemandRevenue() + bidderPerformance.getDemandRevenue());
							existingBidderPerformance.setMargin(existingBidderPerformance.getMargin() + bidderPerformance.getMargin());
						} else {
							dFilterMap.put(key, bidderPerformance);
							//dFilterMap.put(key, bidderPerformance);
						}
					}
					
				} catch (Exception e) {
					logger.error("Error in recordNumber " + recordNumber + " while ingesting dfilter data" + e);
				}
			}
			recordNumber++;
		}
		logger.info("New map generated : " + dFilterMap.size());
	}

	@Override
	public void run() {
		try {
			logger.info("Fetching Data for Dfilter from S3");
			
			String accessKey = Configuration.getConfig().getString("dfilterserver.awsS3accessKey");
			String secretKey = Configuration.getConfig().getString("dfilterserver.awsS3secretKey");
		
			AmazonS3Reader amazonS3Reader = new AmazonS3Reader(accessKey,secretKey);
			
			DFilterCacheBlockSegregatorByQuarter currentCacheBlock = DFilterCacheBlockSegregatorByQuarter.getInstance()
					.getCurrentCacheBlock();
			
			DFilterCacheBlockSegregatorByQuarter nextCacheBlock = DFilterCacheBlockSegregatorByQuarter.getInstance()
					.getNextCacheBlock();
			
			String S3Bucket = Configuration.getConfig().getString(Constants.AWS_DFILTER_S3DUMPBUCKET);
			
			Map<String, DFilterPerformanceValue> currentCacheBlockMap = DFilterCache.getInstance().getDFilterCache(currentCacheBlock);
			Map<String, DFilterPerformanceValue> nextCacheBlockMap = DFilterCache.getInstance().getDFilterCache(nextCacheBlock);
			
			String dfilterPath = Configuration.getConfig().getString(Constants.DFILTER_FILEPATH);
			if (currentCacheBlockMap.isEmpty()) {
				String fileName = this.getS3DFilterFileName(currentCacheBlock);
				String fullFilePath = dfilterPath + "/" + fileName;
				amazonS3Reader.readFromS3ToFile(S3Bucket, fileName, fullFilePath);
				//S3Object s3Object = s3Client.getObject(new GetObjectRequest(S3Bucket, fileName));
				logger.info("Fetched dfilter data with fileName : " + fileName);
				this.loadDFilterDataInCache(new File(fullFilePath), currentCacheBlock);
			}
			
			if (nextCacheBlockMap.isEmpty()) {
				String fileName = this.getS3DFilterFileName(nextCacheBlock);
				String fullFilePath = dfilterPath + "/" + fileName;
				amazonS3Reader.readFromS3ToFile(S3Bucket, fileName, fullFilePath);
				//S3Object s3Object = s3Client.getObject(new GetObjectRequest(S3Bucket, fileName));
				logger.info("Fetched dfilter data with fileName : " + fileName);
				this.loadDFilterDataInCache(new File(fullFilePath), nextCacheBlock);
			}
			
			DFilterCache.getInstance().cleanOldDFilterCache(CACHE_CLEARING_TIME_THRESHOLD_IN_MILLIS);

		} catch (IOException e) {
			logger.error("Error while loading properties for ", e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error while loading properties for ", e);
		}
	}

	public String getS3DFilterFileName(DFilterCacheBlock cacheBlock) {
		String dumpFileName = "dfilter.bidder_predicted_performance_" + cacheBlock.toString();
		return dumpFileName;
	}
	
	public static void main(String[] args) {
		S3dFilterFetcher s3DFilterFetcher = new S3dFilterFetcher();
		s3DFilterFetcher.run();
	}

}
