package com.vdopia.protocol;

public class ObjectServerException extends Exception{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 7580511738832677161L;

	/*
	 * Creates a new instance
	 */
	public ObjectServerException() {
		super();
	}
	
	/*
	 * Creates a new instance
	 */
	public ObjectServerException(Throwable cause) {
		super(cause);
	}

	/*
	 * Creates a new instance
	 */
	public ObjectServerException(String message, Throwable cause) {
		super(message);
	}

}

