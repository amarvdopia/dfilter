package com.vdopia.data.model;

import java.io.Serializable;
import java.util.Date;

public class OLDFilterSettings implements Serializable, DataEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4948647701808629328L;

	private String settingKey;

	private String settingValue;

	private Date lastUpdatedAt;

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

}
