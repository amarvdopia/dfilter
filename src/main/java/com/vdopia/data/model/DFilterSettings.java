package com.vdopia.data.model;

import java.io.Serializable;
import java.util.Date;

import com.vdopia.data.BaseObject;

public class DFilterSettings implements Serializable, BaseObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 67988956836447769L;

	private String settingKey;

	private String settingValue;

	private Date lastUpdatedAt;

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

}
