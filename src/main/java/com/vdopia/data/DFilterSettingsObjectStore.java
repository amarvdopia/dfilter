package com.vdopia.data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.aws.DeserializerException;
import com.vdopia.data.model.DFilterSettings;
import com.vdopia.data.model.OLDFilterSettings;

public class DFilterSettingsObjectStore<T> extends ObjectStore {

	private static final Logger logger = LoggerFactory.getLogger(DFilterSettingsObjectStore.class.getName());
	private static final Logger criticallogger = LoggerFactory.getLogger("critical");

	public static final String _DFilterSettingKey = "__DFilterSettingsKey";
	private volatile ConcurrentHashMap<String, DFilterSettings> dfilterSettingsStore;

	@Override
	public void populateDataStore(String regionName) {
		return;
	}

	@Override
	public int getDuration() {
		return 0;
	}

	public void loadDataStore() throws DeserializerException {

		try {
			List<OLDFilterSettings> apiList = new ArrayList<OLDFilterSettings>();
			apiList = (List<OLDFilterSettings>) readObjectFromFile(ObjectStore.FILE_STORE,
					DFilterSettingsObjectStore.class.getCanonicalName() + ".bin", apiList.getClass());

			if (dfilterSettingsStore == null) {
				dfilterSettingsStore = new ConcurrentHashMap<String, DFilterSettings>();
			}

			logger.info("init apiList {}", apiList.size());
			for (OLDFilterSettings oapi : apiList) {
				DFilterSettingsTransformer pTransformer = new DFilterSettingsTransformer();
				com.vdopia.data.model.DFilterSettings dfil = (DFilterSettings) pTransformer.transform(oapi);
				String apikey = buildDFilterSettingsCacheKey(dfil.getSettingKey());
				dfilterSettingsStore.put(apikey, dfil);
			}

		} catch (Exception e) {
			logger.error("Exception ", e);
			throw new DeserializerException("Error while loading");
		}
		logger.info("dfilterSettingsStore: {}", dfilterSettingsStore.size());
	}
	
	public DFilterSettings getDFilterSettings(String dkey){
		return dfilterSettingsStore.get(buildDFilterSettingsCacheKey(dkey));
	}
	
	private static String buildDFilterSettingsCacheKey(String settingsKey) {
		return settingsKey + _DFilterSettingKey;
	}

}
