package com.vdopia.data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.vdopia.dfilter.DFilterInputData;
import com.vdopia.processors.InventoryKey;

public class InventoryKeyTransformer {

	public List<InventoryKey> tranform(DFilterInputData dfi) {

		List inventoryKeyList = new ArrayList<>();
		List<String> bidderList = dfi.getBidders();
		for (String bidder : bidderList) {
			InventoryKey ikey = new InventoryKey();
			ikey.setApiKey(dfi.getApikey());
			ikey.setBidderId(bidder);
			ikey.setCountry(dfi.getCountry());
			ikey.setMetroCode(dfi.getLocationCode());
			ikey.setPropertyName(dfi.getPropertyName());
			
			LocalDateTime localTime = LocalDateTime.now();
			int currentHourOfDay = 13;
					//localTime.getHour();
			int currentHourQuarter = 3;
					//localTime.getMinute() / 15 + 1;
			int currentDay = 13;
					//localTime.getDayOfMonth();

			ikey.setDay(currentDay+"");
			ikey.setHour(currentHourOfDay + "");
			ikey.setQuarter(currentHourQuarter + "");
			
			inventoryKeyList.add(ikey);
			
		}

		return inventoryKeyList;
	}
	
}
