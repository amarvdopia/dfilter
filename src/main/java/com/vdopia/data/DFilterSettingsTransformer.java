package com.vdopia.data;

import com.vdopia.data.BaseObject;
import com.vdopia.data.model.DataEntity;
import com.vdopia.data.model.OLDFilterSettings;
import com.vdopia.data.model.DFilterSettings;

public class DFilterSettingsTransformer implements DataTransformer {

	@Override
	public BaseObject transform(DataEntity dataEntity) {

		OLDFilterSettings oldFilterSettings = (OLDFilterSettings) dataEntity;
		DFilterSettings copy = new DFilterSettings();
		copy.setSettingKey(oldFilterSettings.getSettingKey());
		copy.setSettingValue(oldFilterSettings.getSettingValue());
		copy.setLastUpdatedAt(oldFilterSettings.getLastUpdatedAt());
		
		return copy;
		
	}
	
}
