package com.vdopia.data;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.bootstrap.Configuration;
import com.vdopia.data.model.OLDFilterSettings;

public class ObjectStoreFactory {

	private static final Logger logger = LoggerFactory.getLogger(ObjectStoreFactory.class.getName());

	private final Map<String, ObjectStore> configDataStores;

	public final static String FILE_STORE = Configuration.getConfig().getString("dfilterserver.filestorepath");
	
	public ObjectStoreFactory(List<String> regionList) {

		this.configDataStores = new LinkedHashMap<String, ObjectStore>();
		this.registerConfigStore(ObjectEnum.DFILTER.name(), new DFilterObjectStore());
		this.registerConfigStore(ObjectEnum.DFILTERSETTING.name(), new DFilterSettingsObjectStore<OLDFilterSettings>());
	}

	public ObjectStore getConfigDataStore(ObjectEnum entity) {
		return configDataStores.get(entity.name());
	}
	
	public void registerConfigStore(String key, ObjectStore dataStore) {
		configDataStores.put(key, dataStore);
	}
	
	/**
	 * @return the dataStores
	 */
	public Map<String, ObjectStore> getConfigDataStores() {
		return configDataStores;
	}
}
