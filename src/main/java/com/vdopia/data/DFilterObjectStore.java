package com.vdopia.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.paukov.combinatorics3.Generator;
import org.paukov.combinatorics3.IGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.aws.AmazonS3Reader;
import com.vdopia.aws.DeserializerException;
import com.vdopia.bootstrap.Configuration;
import com.vdopia.cache.DFilterCacheBlock;
import com.vdopia.dfilter.DFilterKeyGenerator;
import com.vdopia.dfilter.model.BucketType;
import com.vdopia.dfilter.model.DFilterBucketFactory;
import com.vdopia.dfilter.model.DFilterPerformanceValue;
import com.vdopia.dfilter.model.FixedBucket;
import com.vdopia.dfilter.model.FixedBucketKey;
import com.vdopia.dfilter.model.FixedBucketValue;
import com.vdopia.dfilter.model.VariableBucket;
import com.vdopia.dfilter.model.VariableBucketKey;
import com.vdopia.dfilter.model.VariableBucketValue;
import com.vdopia.processors.InventoryKey;
import com.vdopia.util.Constants;

public class DFilterObjectStore<T> extends ObjectStore {

	private static final Logger logger = LoggerFactory.getLogger(DFilterObjectStore.class.getName());
	private static final Logger criticallogger = LoggerFactory.getLogger("critical");

	private final AmazonS3Reader amazonS3Reader;
	private final String awsDfilterCacheFileName;
	private final DFilterBucketFactory dFilterBucketFactory;

	private String s3Bucket;

	public DFilterObjectStore() {
		super();
		
		String accessKey = Configuration.getConfig().getString("dfilterserver.dfilterawsS3accessKey");
		String secretKey = Configuration.getConfig().getString("dfilterserver.dfilterawsS3secretKey");
	
		this.amazonS3Reader = new AmazonS3Reader(accessKey,secretKey);
		this.awsDfilterCacheFileName = Configuration.getConfig().getString(Constants.AWS_DFILTER_CACHE_FILENAME);
		this.dFilterBucketFactory = new DFilterBucketFactory();

		FixedBucket<FixedBucketKey, FixedBucketValue> fixedBucket = (FixedBucket) dFilterBucketFactory
				.getBucket(BucketType.FIXED);
		if (fixedBucket == null) {
			dFilterBucketFactory.registerBucket(BucketType.FIXED, new FixedBucket<FixedBucketKey, FixedBucketValue>());
		}

		VariableBucket<VariableBucketKey, VariableBucketValue> variableBucket = (VariableBucket) dFilterBucketFactory
				.getBucket(BucketType.VAIRABLE);
		if (variableBucket == null) {
			dFilterBucketFactory.registerBucket(BucketType.VAIRABLE,
					new VariableBucket<VariableBucketKey, VariableBucketValue>());
		}

	}

	@Override
	public void populateDataStore(String regionName) {
		try {

			logger.info("Fetching Data for Dfilter from S3");
			
			String bucketNamePrefix = 	Configuration.getConfig().getString(Constants.AWS_DFILTER_S3DUMPBUCKET);
			this.s3Bucket = "dfilter.bidder_predicted_performance_v2_2017_03_09_19_1";
					//DFilterKeyGenerator.getCurrentQuarterTimeKey(bucketNamePrefix);

			// "dfilter.bidder_predicted_performance_v2_2017_03_09_19_1";
					
			logger.info("S3 Bucket {}", this.s3Bucket);

			String dfilterPath = Configuration.getConfig().getString(Constants.DFILTER_FILEPATH);
			String fileName = this.awsDfilterCacheFileName;
			String fullFilePath = dfilterPath + "/" + fileName;

			logger.info("fullFilePath {}", fullFilePath);
			//amazonS3Reader.readFromS3ToFile("dfilter", s3Bucket, fullFilePath);
			logger.info("Fetched dfilter data with fileName : " + fileName);
			this.loadDFilterDataInCache(new File(fullFilePath));

		} catch (IOException e) {
			logger.error("Error while loading properties for ", e);
			criticallogger.error("Error while loading properties for ", e);
		} catch (Exception e) {
			logger.error("Error while loading properties for ", e);
			criticallogger.error("Error while loading properties for ", e);
		}
	}

	private void loadDFilterDataInCache(File newFile) throws IOException {

		logger.info("File length of fetched dfilter data dump = " + newFile.length());

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(newFile)));
		Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(reader);
		int recordNumber = 0;

		logger.info("Starting ingestion of data.");
		
		for (CSVRecord record : records) {
			
			if(recordNumber>=150){
				return;
			}
			
			if (recordNumber > 68) {
				try {
					// TODO Decouple this part 
					// //bidderId,propertyName,apikey,country,hour,quarter,zip,ai,vi,revenue,margin
					
					//logger.info("{}",record);
					
					String bidderId = record.get(0);
					String propertyName = record.get(1);
					String apiKey = record.get(2);

					String country = record.get(3);
					String hour = record.get(4);
					String quarter = record.get(5);
					String metroCode = record.get(6);
					
					int ai = Integer.parseInt(record.get(7));
					int vi = Integer.parseInt(record.get(8).equals("") ? "0" : record.get(8));
					float revenue = Float.parseFloat(record.get(9).equals("") ? "0" : record.get(9));
					float margin = Float.parseFloat(record.get(10).equals("") ? "0" : record.get(10));

					List<String> fkeyList = new ArrayList();
					fkeyList.add(bidderId);
					fkeyList.add(propertyName);
					fkeyList.add(apiKey);
					FixedBucketKey fixedkey = new FixedBucketKey(fkeyList);
					FixedBucketValue fvalue = new FixedBucketValue(fixedkey.getDfilterKey());
					FixedBucket fixedBucket = (FixedBucket) dFilterBucketFactory.getBucket(BucketType.FIXED);
					if (fixedBucket.getProvider().get(fixedkey.getDfilterKey()) == null) {
						logger.info("bkey {} bvalue {}", fixedkey.getDfilterKey(), fvalue.getValue());
						fixedBucket.getProvider().put(fixedkey.getDfilterKey(), fvalue);
					}
					
					List<String> vkeyList = new ArrayList();
					vkeyList.addAll(fkeyList);
					vkeyList.add(country);

					/**
					 * Add formatting here only 
					 */
					vkeyList.add(hour);
					vkeyList.add(quarter);
					vkeyList.add(metroCode);

					VariableBucketKey varkey = new VariableBucketKey(vkeyList);
					VariableBucket varBucket = (VariableBucket) dFilterBucketFactory.getBucket(BucketType.VAIRABLE);

					//logger.info(" varkey.getDfilterKey {}", varkey.getDfilterKey());
					if (varBucket.getProvider().get(varkey.getDfilterKey()) == null) {

						logger.info(" varkey.getDfilterKey {}", varkey.getDfilterKey());
						
						DFilterPerformanceValue dfpvalue = new DFilterPerformanceValue(varkey.getDfilterKey(),bidderId);
						dfpvalue.setAuctions(ai);
						dfpvalue.setImpressions(vi);
						dfpvalue.setDemandRevenue(revenue);
						dfpvalue.setMargin(margin);

						VariableBucketValue avalue = new VariableBucketValue(dfpvalue);
						varBucket.getProvider().put(varkey.getDfilterKey(), avalue);
					}

				} catch (Exception e) {
					logger.error("Error in recordNumber " + recordNumber + " while ingesting dfilter data", e);
				}
			}
			recordNumber++;
		}
		logger.info("End of ingestion");

	}

	public DFilterPerformanceValue lookup(InventoryKey dkey) {

		DFilterPerformanceValue selectedDValue = null;

		String bidderId = dkey.getBidderId();
		String propertyName = dkey.getPropertyName();
		String apikey = dkey.getApiKey();

		List<String> fkeyList = new ArrayList();
		fkeyList.add(bidderId);
		fkeyList.add(propertyName);
		fkeyList.add(apikey);

		FixedBucketKey bkey = new FixedBucketKey(fkeyList);
		FixedBucket fixedBucket = (FixedBucket) dFilterBucketFactory.getBucket(BucketType.FIXED);

		logger.info("lookupkey {}", bkey.getDfilterKey());
		FixedBucketValue bvalue = (FixedBucketValue) fixedBucket.getProvider().get(bkey.getDfilterKey());

		if (bvalue != null) {
			
			BucketType vstate = bvalue.getNextKeyState();
			logger.info("vstate {}", vstate.name());

			while (vstate != BucketType.LAST) {

				VariableBucket vBucket = (VariableBucket) dFilterBucketFactory.getBucket(BucketType.VAIRABLE);

				//VariableBucket vbucket = (VariableBucket) dFilterBucketFactory.getBucket(vstate);
				String country = dkey.getCountry();
				String metroCode = dkey.getMetroCode();

				List<String> vkeyList = new ArrayList();
				vkeyList.addAll(fkeyList);
				vkeyList.add(country);
				
				//vkeyList.add(dkey.getDay());
				vkeyList.add(dkey.getHour());
				vkeyList.add(dkey.getQuarter());
				vkeyList.add(dkey.getMetroCode());
				
				/**
				 * Added support for Collapseing of keys 
				 * Using IGenerator API 
				 */
				//IGenerator
				String mcode = dkey.getMetroCode();
				if(StringUtils.isEmpty(mcode)){
					mcode = "-";
				}
				
				if(StringUtils.isEmpty(country)){
					country = "-";
				}
				
				IGenerator<List<String>>  resList = Generator.permutation(country, dkey.getHour(),dkey.getQuarter(),mcode)
			       .simple();
			    
				TreeSet<DFilterPerformanceValue> dvalueset = new TreeSet<DFilterPerformanceValue>();
				
				for(List<String> singleCombination : resList){
					
					logger.info("singleCombination {}",singleCombination);
					List<String> singlelist = new ArrayList<String>();
					singlelist.addAll(fkeyList);
					singlelist.addAll(singleCombination);
					VariableBucketKey vkey = new VariableBucketKey(singlelist);
				
					logger.info("vkey.getDfilterKey {}",vkey.getDfilterKey());
					VariableBucketValue vvalue = (VariableBucketValue) vBucket.getProvider().get(vkey.getDfilterKey());
					if(vvalue!=null){
						logger.info("vkey.getDfilterKey {}",vvalue.getValue());
						DFilterPerformanceValue dvalue = (DFilterPerformanceValue) vvalue.getValue();
						dvalueset.add(dvalue);
					}
					
				}
			
				if(!dvalueset.isEmpty()){
					selectedDValue = dvalueset.first();
				}
				
				logger.info("selectedDValue {}",selectedDValue);
				vstate = BucketType.LAST;
			}
			
		}
		
		
		logger.info("selectedDValue {}", selectedDValue);

		return selectedDValue;
	}

	public String getS3DFilterFileName(DFilterCacheBlock cacheBlock) {
		String dumpFileName = "dfilter.bidder_predicted_performance_" + cacheBlock.toString();
		return dumpFileName;
	}

	public DFilterBucketFactory getdFilterBucketFactory() {
		return dFilterBucketFactory;
	}
	
	@Override
	public int getDuration() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void loadDataStore() throws DeserializerException {

	}

	public List<DFilterPerformanceValue> filterDemand(List<InventoryKey> dKeyList) {
		/**
		 * Map DemandKey with the Dfilter Cache
		 */
		List<DFilterPerformanceValue> dfilterpvalueList = new ArrayList();

		for (InventoryKey dkey : dKeyList) {
			DFilterPerformanceValue dperformance = this.lookup(dkey);
			if(dperformance!=null){
				dfilterpvalueList.add(dperformance);
			}
		}

		return dfilterpvalueList;
	}

}
