package com.vdopia.data;

import com.vdopia.data.BaseObject;
import com.vdopia.data.model.DataEntity;

public interface DataTransformer {

		public BaseObject transform(DataEntity dataEntity);
		
}
