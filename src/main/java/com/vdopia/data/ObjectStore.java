package com.vdopia.data;

import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdopia.aws.DeserializerException;
import com.vdopia.bootstrap.Configuration;
import com.vdopia.protocol.KryoSerializer;

public abstract class ObjectStore<T> {

	private static final Logger logger = LoggerFactory.getLogger(ObjectStore.class.getName());

	public final static String FILE_STORE = Configuration.getConfig().getString("dfilterserver.filestorepath");

	protected ObjectStore() {

	}

	public abstract void populateDataStore(String regionName);

	public abstract int getDuration();

	public abstract void loadDataStore() throws DeserializerException;
	
	public Object readObjectFromFile(String directory, String fileName, Class objectType) throws DeserializerException {
		try {
			return KryoSerializer.readObjectFromFile(directory, fileName, objectType);
		} catch (FileNotFoundException e) {
			logger.error("Exception in Deserializer: {}", e);
			throw new DeserializerException(e.getMessage());
		}
	}
}
